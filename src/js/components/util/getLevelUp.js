// LevelUp Table

export default function getLevelUp(num) {
  // FIXME: あとで最適化する
  if (2 < num && num < 5) { return 600; }
  else if (4 < num && num < 10) { return 1200; }
  else if (9 < num && num < 20) { return 3800; }
  else if (19 < num && num < 30) { return 9000; }
  else if (29 < num && num < 50) { return 15000; }
  else if (49 < num && num < 100) { return 27000; }
  else if (99 < num && num < 150) { return 60000; }
  else if (149 < num && num < 200) { return 80000; }
  else if (199 < num && num < 250) { return 100000; }
  else if (249 < num && num < 300) { return 120000; }
  else if (299 < num && num < 350) { return 140000; }
  else if (349 < num) { return 160000; }
  else { return 0; }
}