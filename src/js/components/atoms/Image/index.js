import styled from 'styled-components'

const Image = styled.img`
  width: ${props => props.width ? props.width : 'auto'};
  height: ${props => props.height ? props.height : 'auto'};
  max-width: ${props => props.maxWidth ? props.maxWidth : 'initial'}
  max-height: ${props => props.maxHeight ? props.maxHeight : 'initial'}
  min-width: ${props => props.minWidth ? props.minWidth : 'auto'}
  min-height: ${props => props.minHeight ? props.minHeight: 'auto'}
  display: block;
`;

export default Image;