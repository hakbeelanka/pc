import React from 'react'
import styled from 'styled-components'
import Const from '../../../const/'

const Badge = ({...props}) => {
  let value = (props.value > 99) ? '99+': props.value;
  return (
    <Container {...props}>{value}</Container>
  )
}

const Container = styled.p`
  // width: ${props => props.width};
  // height: ${props => props.height};
  ${props => (props.value > 0) ? 'display: flex;':'display: none;'}
  width: ${props => props.small ? '22px': '32px'};
  height: ${props => props.small ? '22px': '32px'};
  ${props => props.border ? `border: 1px solid ${props.border};`: null}
  border-radius: 50%;
  background-color: #ff554e;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: #fff;
  font-size: ${props => {return props.small ? Const.Size.FONT.SMALL : Const.Size.FONT.LOW_MIDDLE} }px;
  line-height: 1em;
  font-weight: bold;
`;

export default Badge;