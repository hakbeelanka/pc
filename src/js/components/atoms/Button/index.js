import styled from 'styled-components'

const Button = styled.button`
  width: ${props => props.width};
  height: ${props => props.height};
  ${props => getButtonShape(props)}
  display: flex;
  justify-contents: flex-start;
  background-color: ${props => props.bgColor ? props.bgColor : null};
  cursor: pointer;
`;

const getButtonShape = props => {
  if (props.rounded) {
    return `border-radius: ${props.height};`
  } else if (props.circle) {
    return `border-radius: 50%`;
  }
}

export default Button;