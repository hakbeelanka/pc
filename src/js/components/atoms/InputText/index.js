import styled from 'styled-components'

const InputText = styled.input.attrs({
  type: 'text',
  size: props => props.small ? 3 : 8,
  placeholder: props => props.placeholder ? props.placeholder : null,
  autoComplete: "on"
})`
  &:focus {
    outline: none;
  }
`;

export default InputText;