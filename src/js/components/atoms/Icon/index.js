import styled from "styled-components"

import CameraWhite from './icons/camera-white.png';
import Cart from './icons/cart.png';
import CartWhite from './icons/cart-white.png';
import Close from './icons/close.png';
import CloseWhite from './icons/close-white.png';
import CollectionWhite from './icons/collection-white.png'
import CollectionInWhite from './icons/collection_in-white.png';
import CheckWhite from './icons/check-white.png';
import HeartWhite from './icons/heart-white.png';
import Logo from './icons/logo.png';
import Menu from './icons/menu.png';
import Search from './icons/search.png';
import SearchBlue from './icons/search_blue.png';
import SearchWhite from './icons/search-white.png';
import Smile from './icons/smile.png';
import SmileWhite from './icons/smile-white.png';
import Yen from './icons/yen.png';
import BarCode from './icons/barcode.png';
import PlusWhite from './icons/plus_white.png';
import DeleteWhite from './icons/delete.png';
import AmazonWhite from './icons/amazon.png';
import BoxInWhite from './icons/box_in-white.png';
import BtnClose from './icons/btn_close.png';
import BtnCloseWhite from './icons/btn_close-white.png';
import BtnCloseMenu from './icons/btn_close-menu.png';
import BtnCheckWhite from './icons/btn_check-white.png';
import BtnSellWhite from './icons/btn_sell-white.png'
import ArrowWhite from './icons/arrow_white.png';
import InboxWhite from './icons/inbox-white.png';

const Icon = styled.span`
  background-image: url(${props => getIconName(props)});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
`;

const getIconName = props => {
  if (props.camera_white) return CameraWhite;
  if (props.cart) return Cart;
  if (props.cart_white) return CartWhite;
  if (props.heart_white) return HeartWhite;
  if (props.logo) return Logo;
  if (props.search) return Search;
  if (props.search_white) return SearchWhite;
  if (props.search_blue) return SearchBlue;
  if (props.smile) return Smile;
  if (props.smile_white) return SmileWhite;
  if (props.yen) return Yen;
  if (props.menu) return Menu;
  if (props.barcode) return BarCode;
  if (props.plus_white) return PlusWhite;
  if (props.delete_white) return DeleteWhite;
  if (props.amazon_white) return AmazonWhite;
  if (props.collection_white) return CollectionWhite;
  if (props.collection_in_white) return CollectionInWhite;
  if (props.box_in_white) return BoxInWhite;
  if (props.btn_close) return BtnClose;
  if (props.btn_close_white) return BtnCloseWhite;
  if (props.btn_checked_white) return BtnCheckWhite;
  if (props.btn_sell_white) return BtnSellWhite;
  if (props.btn_close_menu) return BtnCloseMenu;
  if (props.close) return Close;
  if (props.arrow_white) return ArrowWhite;
  if (props.inbox_white) return InboxWhite;
  if (props.check_white) return CheckWhite;
  if (props.close_white) return CloseWhite;
}

export default Icon;