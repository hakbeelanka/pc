import styled from 'styled-components'
import Const from '../../../const'

import CameraIcon from './camera.png'

const CameraButton = styled.button`
  width: 45px;
  height: 45px;
  border-radius: 50%;
  background: ${Const.Color.BACKGROUND.BLUE};
  display: flex;
  justify-content: center;
  align-items: center;
  &:after {
    content: '';
    background-image: url(${CameraIcon});
    background-size: contain;
    background-repeat: no-repeat;
    width: 29.5px;
    height: 24px;
  }

`;

export default CameraButton;