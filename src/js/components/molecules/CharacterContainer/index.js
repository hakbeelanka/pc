import React,{Component} from 'react'
import styled from 'styled-components'
import Const from '../../../const'

import Image from '../../atoms/Image'
import Balloon from '../../molecules/Balloon'
import Button from '../../atoms/Button'
import Animate, {Tada} from 'animate-css-styled-components';
import Typist from 'react-typist'

class CharacterContainer extends Component {

  constructor(props){
    super(props);
  }

  render(){
    return (
      <Container>
        <Urumi
          src={`/assets/images/urumi.png`}
          maxWidth={`100%`}
          width={`600px`}
        />
        {/*<OldnijiouButton isShadow*/}
                         {/*width={`103px`}*/}
                         {/*height={`103px`}*/}
                         {/*circle*/}
                         {/*bgColor={Const.Color.BACKGROUND.LPINK}*/}
        {/*>*/}
          {/*にじおう！<br />本サイトへ*/}
        {/*</OldnijiouButton>*/}
        <BalloonContainer>
          {/* FIXME: 初期は適当なセリフ何個かをループさせておく */}
          {
            this.props.user.isUser && this.props.apps.characterMessage.length !== 0 ? (
              <Animate Animation={[Tada]}
                       duration={["0.7s"]}
                       delay={[`0.2s`]}>
                <Balloon isShadow
                         arrow={'top'}
                         name={`うるみ`}
                         message
                         bgColor={Const.Color.BACKGROUND.A93WHITE}
                         onClick={e => window.location = 'https://nijigen-okoku.jp/'}
                >
                  <Message>
                    {/*<Typist*/}
                    {/*cursor={{show: false}}*/}
                    {/*startDelay={500}*/}
                    {/*stdTypingDelay={15}*/}
                    {/*avgTypingDelay={15}*/}
                    {/*key={this.props.apps.characterMessage}*/}
                    {/*/>*/}
                    {this.props.apps.characterMessage}
                  </Message>
                </Balloon>
              </Animate>
            ) : null
          }
        </BalloonContainer>
      </Container>
    )
  }
}

const Container = styled.div``;
const Urumi = Image.extend``;
const BalloonContainer = styled.div`
  position: absolute;
  top: 350px;
  left: 50%;
  transform: translateX(-50%);
  min-width: 100px;
`;

const Message = styled.div`
  margin: 10px 0 0;
  // font-family: "DSきりぎりす","DS-kirigirisu", serif;
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  color: #000;
  line-height: 1.2em;
`;


export default CharacterContainer;