import React from 'react'
import styled from 'styled-components'
import Const from '../../../const/'

import Icon from '../../../components/atoms/Icon'
import Button from '../../../components/atoms/Button'
import {Label} from '../../../components/atoms/Text'
import CountUp from 'react-countup'

// FIXME: あとで再利用可能に修正する

const LandscapeCard = ({...props}) => {

  return (
    <Container {...props}>
      <CloseButton
        onClick={e => props.removeMyCart(props.selectKey)}
      >
        <CloseIconWrap>
          <CloseIcon close_white />
        </CloseIconWrap>
      </CloseButton>
      <Inner>
        <ItemImage poster={props.product_image}/>
        <ItemInfo>
          <ItemProductName>{props.product_name}</ItemProductName>
          <ItemMakerName>{props.maker_name}</ItemMakerName>
        </ItemInfo>
        <ItemPriceContainer>
          <ItemPriceLabel>買取価格</ItemPriceLabel>
          <ItemPrice>
            {
              props.exercise_price !== '0' ? (
                <CountUp end={
                  !props.isUnopened ?
                    parseInt(props.exercise_price) :
                    parseInt(props.exercise_price) * 1.1
                }
                         separator=","
                />
              ) : (
                <span>-</span>
              )
            }
          </ItemPrice>
        </ItemPriceContainer>
        <UnopenedButton onClick={e => {
          if(!props.isUnopened) {
            props.checkUnopenedCollection(props.selectKey)
          } else {
            props.uncheckUnopenedCollection(props.selectKey)
          }
        }}>
          {
            !props.isUnopened ? (
              <IconWrap>
                <UnopenedIcon check_white />
              </IconWrap>
            ) : (
              <IconWrap check_active>
                <UnopenedIcon check_white />
              </IconWrap>
            )
          }
          <UnopenedLabel>新品未開封</UnopenedLabel>
        </UnopenedButton>
      </Inner>
    </Container>
  )

}

const Container = styled.div`
  position: relative;
  height: 120px;
  padding: 10px;
  background-color: ${Const.Color.BACKGROUND.A93WHITE};
  border-radius: 5px;
`;

const CloseIconWrap = styled.div`
  position: relative;
  width: 35px;
  height: 35px;
  border-radius: 50%;
  background-color: ${Const.Color.BACKGROUND.LBLUE};
  &:hover {
    background-color: ${Const.Color.BACKGROUND.DGRAY};
  }
`;

const CloseIcon = styled(Icon)`
  position: absolute;
  width: 13px;
  height: 13px;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
`;

const CloseButton = Button.extend`
  position: absolute;
  top: 50%;
  left: 12px;
  transform: translateY(-50%);
  cursor: pointer;
`;

const Inner = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  width: calc(100% - 57px);
  height: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const ItemImage = styled.div`
  width: 130px;
  height: 100%;
  
  ${props => {
    return (props.poster) ?
    `background-image: url(${props.poster});` :
    `background-image: url(/assets/images/noimage.png);`
  }}
  background-size: cover;
  background-repeat: no-repeat;
  background-position: top center;
`;

const ItemInfo = styled.div`
  width: 240px;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  padding: 0 0 0 20px;
`;

const ItemProductName = Label.extend`
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.PRIMARY};
`;

const ItemMakerName = Label.extend`
  font-size: ${Const.Size.FONT.SMALL}px;
  color: ${Const.Color.TEXT.WEAK};
  margin: 8px 0 0;
`;

const ItemPriceContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  height: 100%;
  padding: 0 0 0 30px;
`;

const ItemPriceLabel = Label.extend`
  display: block;
  font-size: ${Const.Size.FONT.BASE}px;
  font-weight: normal
  color: ${Const.Color.TEXT.SECONDARY};
  margin: 8px 0 0;
`;

const ItemPrice = Label.extend`
  font-size: ${Const.Size.FONT.LARGE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.SECONDARY};
  &:before {
    content: "¥";
    font-size: ${Const.Size.FONT.MIDDLE}px;
    font-weight: bold;
    color: ${Const.Color.TEXT.SECONDARY}; 
  }
`;

const UnopenedButton = Button.extend`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  top: 50%;
  right: 20px;
  transform: translateY(-50%);
`;

const UnopenedIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  width: 17px;
  height: 12px;
`;

const UnopenedLabel = Label.extend`
  margin: 8px 0 0;
  color: ${Const.Color.TEXT.PRIMARY};
`;

const IconWrap = styled.div`
  position: relative;
  margin: 0 auto;
  width: 29px;
  height: 29px;
  border-radius: 50%;
  ${props => {
  return !props.check_active ?
    `background-color: ${Const.Color.BACKGROUND.LBLUE};` :
    `background-color: ${Const.Color.BACKGROUND.DBLUE};`
}
  };  
`;


export default LandscapeCard;