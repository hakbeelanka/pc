import React from 'react'
import styled from 'styled-components'
import Const from '../../../const/'

// FIXME: あとで再利用可能に修正する

const Balloon = ({...props}) => {

  return (
    <Container {...props}>
      {
        props.name ?
          (
            <Name>{props.name}</Name>
          ) :
          null
      }
      {
        props.children
      }
    </Container>
  )

}

const Container = styled.div`
  position: relative;
  height: auto;
  padding: 10px;
  border-radius: 20px;
  ${props => props.isShadow ?
    `box-shadow: 0 6px 8px ${Const.Color.SHADOW.A73BLACK};` :
    null
  }
  ${props => props.bgColor ?
    `background-color: ${props.bgColor};` :
    `background-color: ${Const.Color.BACKGROUND.A93WHITE};`
  }
  &:before {
    ${props => getArrowShape(props)}
    position: absolute;
    font-family: sans-serif;
    color: ${props => props.bgColor ? props.bgColor : Const.Color.BACKGROUND.A93WHITE };
    
  }
`;

const Name = styled.p`
  display: inline;
  height: 20px;
  padding: 4px;
  border-radius: 20px;
  background: #ff6292;
  color: #fff;
  font-size: ${Const.Size.FONT.BASE}px;
  text-align: center;  
  // font-family: "DSきりぎりす","DS-kirigirisu", serif;
  font-family: "waon_joyo", serif;
`;

const getArrowShape = props => {
  switch(props.arrow){
    case 'top':
      return `
        content: '▲';
        left: 50%;
        width: auto;
        height: auto;   
        top: -27px;
        font-size: 30px;
        transform: translateX(-50%);
      `;
    case 'bottom':
      return `
        content: '▼';
        text-shadow: 0 6px 8px ${Const.Color.SHADOW.A73BLACK};
        left: 50%;
        width: auto;
        height: auto;   
        bottom: -26px;
        font-size: 30px;
        transform: translateX(-50%);
      `;
    case 'left':
      return `
        content: '◀︎';
        text-shadow: -3px 4px 6px ${Const.Color.SHADOW.A73BLACK};
        top: 50%;
        left: -23px;
        width: auto;
        height: auto; 
        font-size: 30px;
        transform: translateY(-50%);
      `;
    case 'right':
      return `
        content: '▶︎';
        text-shadow: 3px 4px 6px ${Const.Color.SHADOW.A73BLACK};
        top: 50%;
        right: -23px;
        width: auto;
        height: auto; 
        font-size: 30px;
        transform: translateY(-50%);
      `;
    default:
      return ``;
  }
}

export default Balloon;