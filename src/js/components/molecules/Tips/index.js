import React,{Component} from 'react'
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import Const from '../../../const'
import Image from '../../../components/atoms/Image'
import BalloonComponent from '../../../components/molecules/Balloon'
import Button from "../../atoms/Button";
import Icon from "../../atoms/Icon";
import {
  closeTips
} from '../../../redux/modules/apps'

class Tips extends Component {

  constructor(props){
    super(props);
    this.state = {
      currentKey: '',
      tips: [
        {
          key: 'speedlink',
          render: (
            <Image width={`100%`} src={`/assets/images/speed.png`}/>
          ),
          text: (<Text>とにかく<Strong>すぐに宅配買取</Strong>を申し込みたい方はコチラ！</Text>),
          plotarea: 'inner',
          next: 'keywordsearch',
          arrow: "top"
        },
        {
          key: 'keywordsearch',
          render: (
            <Image src={`/assets/images/tips_2.png`}/>
          ),
          text: (<Text><Strong>フィギュア名やシリーズ名、アニメタイトル</Strong>などから検索してみてくださいね！</Text>),
          plotarea: 'functions',
          next: 'collectionbutton',
          arrow: "bottom"
        },
        {
          key: 'collectionbutton',
          render: (
            <Image src={`/assets/images/tips_3.png`}/>
          ),
          text: (<Text>あなたの持っているフィギュアを<Strong>コレクションに追加</Strong>しちゃおう！<br />にじおうサイトに<Strong>あなただけのショーケース</Strong>を作れるよ♪</Text>),
          plotarea: 'functions',
          next: 'applybutton',
          arrow: "bottom"
        },
        {
          key: 'applybutton',
          render: (
            <Image src={`/assets/images/tips_4.png`}/>
          ),
          text: (<Text>あなたの持っているものを<Strong>「うるカート」</Strong>に入れてみてください！<Strong>「うるカート」</Strong>買取シミュレーションができます！</Text>),
          plotarea: 'functions',
          next: "end",
          arrow: "bottom"
        }
      ]
    }
    this.nextTips = this.nextTips.bind(this);
  }

  componentDidMount(){
    this.setState({
      currentKey: 'speedlink'
    })
  }

  nextTips(key){
    if (key !== "end"){
      this.setState({
        currentKey: key
      })
    } else {
      // Tipsモード終了の描画
      this.props.closeTips()
    }
  }

  render(){

    let render;

    return (
      <Overlay {...this.props}>
        <InnerPlotArea>
          <Inner>
            {
              this.state.tips.map(item => {
                return this.props.apps.tips.map(v => {
                  if (item.key === v.key && item.key === this.state.currentKey && item.plotarea === 'inner'){
                    render = (
                      <Renderer key={`tip`} pos={v.pos} style={{width: `100%`}}>
                        {item.render}
                        <Balloon isShadow
                                 arrow={item.arrow}
                        >
                          {item.text}
                          <NextButton circle
                                      width={`30px`}
                                      height={`30px`}
                                      bgColor={Const.Color.BACKGROUND.D2GRAY}
                                      onClick={e => this.nextTips(item.next)}
                          >
                            <ArrowIcon arrow_white />
                          </NextButton>
                        </Balloon>
                      </Renderer>
                    );
                    return render;
                  }
                })
              })
            }
          </Inner>
        </InnerPlotArea>
        <FunctionsPlotArea>
          {
            this.state.tips.map(item => {
              return this.props.apps.tips.map(v => {
                if (item.key === v.key && item.key === this.state.currentKey && item.plotarea === 'functions'){
                  render = (
                    <Renderer key={`tip`} pos={v.pos}>
                      {item.render}
                      <Balloon isShadow
                               arrow={item.arrow}
                      >
                        {item.text}
                        <NextButton circle
                                    width={`30px`}
                                    height={`30px`}
                                    bgColor={Const.Color.BACKGROUND.D2GRAY}
                                    onClick={e => this.nextTips(item.next)}
                        >
                          <ArrowIcon arrow_white />
                        </NextButton>
                      </Balloon>
                    </Renderer>
                  );
                  return render;
                }
              })
            })
          }
        </FunctionsPlotArea>
      </Overlay>
    );
  }
}

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1200;
  background-color: ${Const.Color.BACKGROUND.A80BLACK};
  ${props => {return props.apps.isOpenTips ? `display: block` : `display: none`}}
`;

const InnerPlotArea = styled.div`
  position: absolute;
  top: 70px;
  max-width: 1680px;
  width: 89.6vw;
  min-width: 1120px;
  height: 100%;
  left: 50%;
  transform: translateX(-50%);
`;

const Inner = styled.div`
  position: absolute;
  max-width: 987px;
  width: 58.75vw;
  min-width: 735px;
  height: 100%;
  top: 0;
  left: 0;
`;

const FunctionsPlotArea = styled.div`
  position: absolute;
  bottom: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 1020px;
  height: 117px;
`;

const Text = styled.p``;

const Renderer = styled.div`
  position: absolute;
  ${props => {return `
    top: ${props.pos[1]}px;
    left: ${props.pos[0]}px;
  `}}
`;

const Balloon = styled(BalloonComponent)`
  position: absolute;
  padding: 30px;
  width: 300px;
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.BASE}px;
  line-height: 1.4em;
  color: ${Const.Color.TEXT.PRIMARY};
  ${props => {
    switch (props.arrow) {
      case "top": 
        return `
          margin: 10px 0 0;
          left: 50%;
          transform: translateX(-50%);
          &:before {
            top: -20px;
          }
        `;
      case "bottom":
        return `
          margin: 10px 0 0;
          top: -147px;
          left: 50%;
          transform: translateX(-50%);
          &:before {
            bottom: -21px;
          }
        `;
      default:
        return ``;
    }
  }}
`;

const NextButton = Button.extend`
  position: absolute;
  bottom: 17px;
  right: 17px;
`;
const ArrowIcon = styled(Icon)`
  width: 9px;
  height: 14px;
  position: absolute;
  top: 50%;
  left: calc(50% + 2px);
  transform: translate(-50%, -50%) rotate(180deg);
`;

const Strong = styled.span`
  color: ${Const.Color.TEXT.SECONDARY};
`;

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    closeTips
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tips);