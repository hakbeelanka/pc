import React, {Component} from 'react'
import styled from 'styled-components'
import Const from '../../../const'
import Animate, {Bounce} from 'animate-css-styled-components'
import {filter} from 'lodash'
import {Link} from 'react-router-dom'

import Icon from '../../atoms/Icon'
import {Label} from '../../atoms/Text'
import Button from '../../atoms/Button'
import CountUp from 'react-countup'

class ProductCard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isControllerShow: false
    }
  }

  mouseEnterHandle() {
    this.setState({isControllerShow: true})
  }

  mouseLeaveHandle() {
    this.setState({isControllerShow: false})
  }

  render() {
    return (
      <Container {...this.props}
                 onMouseEnter={() => this.mouseEnterHandle()}
                 onMouseLeave={() => this.mouseLeaveHandle()}
      >
        <ProductContainer {...this.props}>
          {this.state.isControllerShow ?
            (
              <ControllerAnimate Animation={[Bounce]}
                                 duration={["0.1s"]}
              >
                <ControllerContainer {...this.props}>
                  <ControlButton onClick={e => {
                    this.props.removeMyCollection(this.props.selectKey)
                    this.props.calculateTotalAmount();
                  }}
                  >
                    <IconWrap>
                      <DeleteIcon delete_white/>
                    </IconWrap>
                    <IconLabel>もうない…</IconLabel>
                  </ControlButton>
                  {
                    !this.props.user.collection[this.props.selectKey].isSell ? (
                      <ControlButton onClick={e => {
                        let count = 0;
                        filter(this.props.user.collection, o => { return o.isSell }).length > 0 ?
                        filter(this.props.user.collection, o => { return o.isSell }).reduce((p, c) => {
                          if (c.jan_code === this.props.jan_code) { count++ }
                        }) : null;
                        if (count > 1) {
                          // 追加できない
                        } else {
                          this.props.addMyCart(this.props.selectKey)
                        }
                      }}>
                        <IconWrap>
                          <CartIcon cart_white/>
                        </IconWrap>
                        <IconLabel>うるかも！</IconLabel>
                      </ControlButton>
                    ) : (
                      <ControlButton
                        // onClick={e => {this.props.removeMyCart(this.props.selectKey)}}
                      >
                        <IconWrap check_active>
                          <CheckIcon check_white/>
                        </IconWrap>
                        <IconLabel>うるカート入</IconLabel>
                      </ControlButton>
                    )
                  }
                  <ProductDetailContainer>
                    <ProductPrice>
                      {
                        this.props.exercise_price !== '0' ? (
                          <CountUp end={parseInt(this.props.exercise_price.replace(',', ''))}
                                   duration={0.01}
                                   separator=","
                                   redraw={true}
                          />
                        ) : (
                          <span>-</span>
                        )
                      }
                    </ProductPrice>
                    <DetailButton rounded
                                  width="57px"
                                  height="23px"
                                  bgColor={Const.Color.BACKGROUND.BLUE}
                    ><Link to={{
                      pathname: `/details/${this.props.product_id}?key=${this.props.selectKey}`,
                      state: {modal: true}
                    }}>詳細</Link></DetailButton>
                  </ProductDetailContainer>
                </ControllerContainer>
              </ControllerAnimate>
            ) :
            null
          }
        </ProductContainer>
      </Container>
    )
  }
}

const Container = styled.div`
  position: relative;
  background-color: ${Const.Color.BACKGROUND.WHITE};
  width: 138.6px;
  height: 190px;
  border-bottom: 1px solid ${Const.Color.BORDER.GRAY}
  cursor: grab;
  &:before {
    ${props => props.up ? 'display: flex;' : 'display: none;'}
    position: absolute;
    content: 'UP!';
    font-size: ${Const.Size.FONT.SMALL}px;
    color: ${Const.Color.TEXT.WHITE};
    justify-content: center;
    align-items: center;
    top: 10px;
    left: 10px;
    background-color: ${Const.Color.BACKGROUND.PINK};
    height: 21px;
    width: 42px;
    border-radius: 21px;
  }
  &:after {
    position: absolute;
    content: '';
    width: 100%;
    height: 40px;
    bottom: 0;
    left: 0;
    background: linear-gradient(to bottom, rgba(220,224,225,0) 0%,rgba(220,224,225,0.65) 100%);
    opacity: 0.68;
  }
`;

const ProductContainer = styled.div`
  ${props => {
  return (props.product_image.length !== 0) ?
    `background-image: url(${props.product_image});` :
    `background-image: url(/assets/images/noimage.png);`
  }}
  background-size: cover;
  background-repeat: no-repeat;
  background-position: top center;
  height: 100%;
  ${props => {
    return props.isUp ? `
      &:before {
        content: 'UP!';
        position: absolute;
        top: 3px;
        left: 3px;
        height: 21px;
        border-radius: 21px;
        padding: 5px 8px;
        font-size: ${Const.Size.FONT.SMALL}px;
        color: ${Const.Color.TEXT.WHITE};
        background-color: ${Const.Color.BACKGROUND.PINK};
        display: flex;
        justify-content: center;
        align-items: center;              
      }
    `: null
  }}
`;

const ControllerContainer = styled.div`
  display: block;
  width: 180px;
  height: 140px;
  background-color: ${Const.Color.BACKGROUND.A80BLACK};
  box-shadow: 0 6px 8px ${Const.Color.SHADOW.A73BLACK};
  border-radius: 30px;
  &:before {
    content: '';
    position: absolute;
    width: 20px;
    height: 13px;
    border-bottom: 13px solid ${Const.Color.BACKGROUND.A80BLACK};
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    top: -13px;
    left: 50%;
    transform: translateX(-50%); 
  }
`;

const DeleteIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  width: 13px;
  height: 13px;
  
`;

const CartIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  width: 24px;
  height: 19px;
`;

const CheckIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  width: 17px;
  height: 12px;
`;

const IconWrap = styled.div`
  position: relative;
  margin: 0 auto;
  width: 38px;
  height: 38px;
  border-radius: 50%;
  ${props => {
    return !props.check_active ?
      `background-color: ${Const.Color.BACKGROUND.GRAY};` :
      `background-color: ${Const.Color.BACKGROUND.LPINK};`
    }
  };  
`;

const IconLabel = Label.extend`
  margin: 6px 0 0;
  color: ${Const.Color.TEXT.WHITE};
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.BASE}px;
`;

const ControlButton = styled.button`
  width: 50%;
  height: 90px;
  cursor: pointer;
`;

const ProductDetailContainer = styled.div`
  width: 100%;
`;

const ProductPrice = Label.extend`
  position: absolute;
  bottom: 17px;
  left: 13px;
  font-size: ${Const.Size.FONT.MIDDLE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.WHITE};
  &:before {
    content: "¥";
    font-size: ${Const.Size.FONT.BASE}px;
    color: ${Const.Color.TEXT.WHITE};
  }
`;

const DetailButton = Button.extend`
  position: absolute;
  bottom: 17px;
  right: 13px;
  color: ${Const.Color.TEXT.WHITE};
  font-size: ${Const.Size.FONT.BASE}px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  cursor: pointer;
  a {
    text-decoration: none;
    color: ${Const.Color.TEXT.WHITE};
  }
`;

const ControllerAnimate = styled(Animate)`
  display: block;
  position: absolute;
  bottom: -50%;
  left: 50%;
  margin-left: -90px;
  width: 180px;
  z-index: 10;
`;

export default ProductCard;