import React, {Component} from 'react'
import styled,{css} from 'styled-components'
import Const from '../../../const'
import SlickSlider from 'react-slick'
import Image from '../../../components/atoms/Image'

// FIXME: 未実装・SliderをWrapする必要がある？

class Slider extends Component {

  render() {
    console.log(this.props)
    return (
      <SlickSlider {...this.props.slickOption}>
        {
          this.props.renderData.map((item, i) => {
            return (
              <SliderItem key={i}
                          bgimg={item.img}
                          itemWidth={this.props.itemWidth}
                          itemHeight={this.props.itemHeight}
              />
            )
          })
        }
      </SlickSlider>
    )
  }
}

const SliderItem = styled.div`
  width: ${props => {return props.itemWidth}};
  height: ${props => {return props.itemHeight}};
  background-image: url(${props => {return props.bgimg}});
  background-repeat: no-repeat;
  background-size: contain;
`;


export default Slider;