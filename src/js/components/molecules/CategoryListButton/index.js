import React from 'react'
import styled from 'styled-components'
import Const from '../../../const'

import {Label} from '../../../components/atoms/Text'

const CategoryListButton = ({...props}) => {

  return (
    <Container poster={props.poster}
               onClick={e => {props.history.push(props.to)}}
    >
      <Title bgColor={props.bgColor}>
        <ExtendLabel>{props.title}{props.sub ? (<span>{props.sub}</span>) : null}</ExtendLabel>
      </Title>
    </Container>
  )
}

const Container = styled.button`
  position: relative;
  background-image: url(${props => {return props.poster}});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 180px;
  border-radius: 3px;
  cursor: pointer;
`;

const Title = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  color: ${Const.Color.TEXT.WHITE};
  background-color: ${props => {return Const.Color.BACKGROUND[props.bgColor]}};
  display: flex;
  justify-content: center;
  align-item: center;
  padding: 15px 0;
`;

const ExtendLabel = Label.extend`
  color: ${Const.Color.TEXT.WHITE};
  font-size: ${Const.Size.FONT.LARGE}px;
  font-weight: bold;
  span {
    font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  }
`;

export default CategoryListButton;