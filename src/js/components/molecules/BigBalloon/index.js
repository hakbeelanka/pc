import React,{Component} from 'react'
import styled from 'styled-components'
import Typist from 'react-typist'
import Const from '../../../const/'

// FIXME: あとで再利用可能に修正する

class BigBalloon extends Component {

  constructor(props){
    super(props);
    this.state = {

    }
  }

  render() {
    const props = this.props;
    return (
      <Container {...props}>
        <Inner>
          {this.props.children}
        </Inner>
      </Container>
    )
  }

}

const Container = styled.div`
  width: 695px;
  height: 100%;
  // max-height: 450px;
  padding: 10px;
  background-color: ${Const.Color.BACKGROUND.A93BLUE};
  border-radius: 5px;
  ${props => props.isShadow ?
    `box-shadow: 0 6px 8px ${Const.Color.SHADOW.A73BLACK};` :
    null
  }
  &:before {
    position: absolute;
    content: '▼';
    color: ${Const.Color.BACKGROUND.A93BLUE};
    text-shadow: 0 6px 8px ${Const.Color.SHADOW.A73BLACK};
    left: ${props => props.left}px;
    width: auto;
    height: auto;   
    bottom: -25px;
    font-size: 30px;
  }
  &:after {
    position: absolute;
    bottom: 10px;
    left: 50%;
    content: '';
    width: calc(100% - 20px);
    height: 50px;
    transform: translateX(-50%);
    background: linear-gradient(to bottom, rgba(125,185,232,0) 0%, ${Const.Color.BACKGROUND.A93BLUE} 100%);
  }
`;

const Inner = styled.div`
  position: absolute;
  width: calc(100% - 20px);
  height: calc(100% - 20px);
  overflow-y: scroll;
  top: 10px;
  left: 10px;
`;

const getArrowDirection = props => {
  // console.log(props.arrowBottomCenter,props.arrowTopCenter)
  switch(props) {
    case props.arrowTopCenter:
      console.log('artc')
      return `
        top: -13px;
        left: 50%;
        transform: translateX(-50%);
      `;
    case props.arrowBottomCenter:
      console.log('arbc')
      return `
        bottom: 13px;
        left: 50%;
        transform: rotate(180deg) translateX(-50%);
      `;
    default:
      console.log('def')
      return `
        top: -13px;
        left: 50%;
        transform: translateX(-50%);
      `;
  }
}



export default BigBalloon;