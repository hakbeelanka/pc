import React from 'react'
import styled from 'styled-components'
import Const from '../../../const'
import {
  filter,
  map
} from "lodash";

import Button from '../../../components/atoms/Button'
import Balloon from '../../../components/molecules/Balloon'
import Icon from '../../../components/atoms/Icon'
import {Label} from '../../../components/atoms/Text'
import BadgeComponent from '../../../components/atoms/Badge'
import CountUp from 'react-countup'
import getLevelUp from "../../util/getLevelUp";
import Animate, { Bounce } from 'animate-css-styled-components';

const ApplyButton = ({...props}) => {
  return (
    <Container>
      <ColumnContainer>
        <CartButton width={`64px`}
                    height={`64px`}
                    bgColor={Const.Color.BACKGROUND.LPINK}
                    circle
                    onClick={e => props.history.push('/mycart')}
        >
          <CartIcon cart_white>
            <Badge small
                   border={Const.Color.BORDER.WHITE}
                   value={filter(props.user.collection,(o)=>{ return o.isSell }).length}
            />
          </CartIcon>
        </CartButton>
        <CartAmount value={
          (props.user.collection.length !== 0 && filter(props.user.collection,(o)=>{ return o.isSell }).length !== 0) ? (getLevelUp(map(filter(props.user.collection,(o)=>{ return o.isSell && parseInt(o.exercise_price) + 1 > 1000 })).length) + map(filter(props.user.collection,(o)=>{ return o.isSell }), getIsUnopenedValue).reduce((accumulator, current) => { return accumulator + current; })) : 0}
        >
          <CountUp end={
            (props.user.collection.length !== 0 && filter(props.user.collection,(o)=>{ return o.isSell }).length !== 0) ? (getLevelUp(map(filter(props.user.collection,(o)=>{ return o.isSell && parseInt(o.exercise_price) + 1 > 1000 })).length) + map(filter(props.user.collection,(o)=>{ return o.isSell }), getIsUnopenedValue).reduce((accumulator, current) => { return accumulator + current; })) : 0}
                   separator=","
          />
        </CartAmount>
        {/*{*/}
          {/*<AnimationContainer>*/}
            {/*<Animate Animation={[Bounce]}*/}
                                             {/*duration={["0.1s"]}*/}
            {/*>*/}
              {/*<Balloon bgColor={Const.Color.BACKGROUND.LYELLOW}*/}
                       {/*isShadow*/}
              {/*>*/}
                {/*{*/}
                  {/*getLevelUp(map(filter(props.user.collection,(o)=>{ return o.isSell })).length)*/}
                {/*}*/}
              {/*</Balloon>*/}
            {/*</Animate>*/}
          {/*</AnimationContainer>*/}
        {/*}*/}
      </ColumnContainer>
      <ColumnContainer>
        <SellButton width={`100%`}
                    height={`100%`}
                    onClick={e => {
                      props.history.push('/mycart');
                    }}
        >買取申し込みへ</SellButton>
      </ColumnContainer>
    </Container>
  )
}

const Container = styled.div`
  width: 536px;
  height: 95px;
  border-radius: 95px;
  background-color: ${Const.Color.BACKGROUND.WHITE};
  box-shadow: 0 3px 0 ${Const.Color.SHADOW.A36BLACK};
  display: flex;
  justify-content: space-between;
  align-items: center;
`;


const CartAmount = styled(Label)`
  position: absolute;
  top: 50%;
  right: 15px;
  transform: translateY(-50%);
  margin: -3px 0 0;
  font-size: ${Const.Size.FONT.HIGH_LARGE}px;
  font-weight: bold;
  color: ${props => {return (props.value !== 0) ?
    Const.Color.TEXT.SECONDARY :
    Const.Color.TEXT.WEAK}
  };
  &:before {
    display: inline;
    content: '¥';
    font-size: ${Const.Size.FONT.LARGE}px;
  }
`;

const Badge = styled(BadgeComponent)`
  position: absolute;
  top: -15px;
  right: -10px;
`;

const CartButton = Button.extend`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  left: 12px;
`;


const CartIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 42px;
  height: 41px;
`;

const SellButton = Button.extend`
  background-color: ${Const.Color.BACKGROUND.LPINK};
  font-size: ${Const.Size.FONT.MIDDLE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.WHITE};
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 0 95px 95px 0;
`;

const ColumnContainer = styled.div`
  position: relative;
  width: 50%;
  height: 100%;
`;

const AnimationContainer = styled.div`
  position: absolute;
  top: -40%;
  left: 50%;
  transform: translateX(-50%);
`;

const getIsUnopenedValue = v => {
  return !v.isUnopened ?
    parseInt(v.exercise_price) :
    parseInt(v.exercise_price) * 1.1;
}

export default ApplyButton;
