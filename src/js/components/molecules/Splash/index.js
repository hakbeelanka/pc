import React,{Component} from 'react'
import styled from 'styled-components'
import Const from '../../../const'
import Animate, {ZoomIn,Shake} from 'animate-css-styled-components'
import Icon from '../../../components/atoms/Icon'
import BalloonComponent from '../../../components/molecules/Balloon'
import Typist from 'react-typist'
import Button from '../../../components/atoms/Button'
import ImageComponent from '../../../components/atoms/Image'

class Splash extends Component {

  constructor(props){
    super(props);
    this.state = {
      steps: 0,
      stepsContent: [
        {
          "step": 1,
          "content": (
            <Balloon key={`spl`}
                     isShadow
                     arrow={"right"}
                     padding={`70px`}
            >
              <Typist
              cursor={{show: false}}
              startDelay={500}
              stdTypingDelay={15}
              avgTypingDelay={15}
              >
                二次元美少女王国<br /><Strong>「にじおう」</Strong>へようこそ！
              </Typist>
              <NextButton circle
                          width={`30px`}
                          height={`30px`}
                          bgColor={Const.Color.BACKGROUND.D2GRAY}
                          onClick={e => this.nextStep(2)}
              >
                <ArrowIcon arrow_white />
              </NextButton>
            </Balloon>
          )
        },
        {
          "step": 2,
          "content": (
            <Balloon key={`spl`}
                     isShadow
                     arrow={"right"}
            >
              <Image width={`100%`}
                     src={`/assets/images/splash_3.png`}
              />
              <Typist
                cursor={{show: false}}
                startDelay={500}
                stdTypingDelay={15}
                avgTypingDelay={15}
              >
                にじおう！では、あなたのフィギュアコレクションを<Strong>登録、管理</Strong>できるよ！
              </Typist>
              <NextButton circle
                          width={`30px`}
                          height={`30px`}
                          bgColor={Const.Color.BACKGROUND.D2GRAY}
                          onClick={e => this.nextStep(3)}
              >
                <ArrowIcon arrow_white />
              </NextButton>
            </Balloon>
          )
        },
        {
          "step": 3,
          "content": (
            <Balloon key={`spl`}
                     isShadow
                     arrow={"right"}
            >
              <Image width={`100%`}
                     src={`/assets/images/splash_4.png`}
              />
              <p>
                コレクションに登録すると、今の<Strong>買取相場がすぐにわかる！</Strong>買取キャンペーンでさらに<Strong>最大１６万円</Strong>アップ保証！最短<Strong>即日入金！</Strong>
              </p>
              <NextButton circle
                          width={`30px`}
                          height={`30px`}
                          bgColor={Const.Color.BACKGROUND.D2GRAY}
                          onClick={e => this.nextStep(4)}
              >
                <ArrowIcon arrow_white />
              </NextButton>
            </Balloon>
          )
        },
        {
          "step": 4,
          "content": (
            <Balloon key={`spl`}
                     isShadow
                     arrow={"right"}
                     padding={`70px`}
            >
              <Typist
                cursor={{show: false}}
                startDelay={500}
                stdTypingDelay={15}
                avgTypingDelay={15}
              >
                さあ、あなたのフィギュアの<Strong>総額を今すぐチェック！</Strong>
              </Typist>
              <NextButton circle
                          width={`30px`}
                          height={`30px`}
                          bgColor={Const.Color.BACKGROUND.D2GRAY}
                          onClick={e => {
                            this.props.welcomeApps()
                            this.props.openTips()
                          }}
              >
                <ArrowIcon arrow_white />
              </NextButton>
            </Balloon>
          )
        },
      ]
    }
    this.nextStep = this.nextStep.bind(this);
    this.skip = this.skip.bind(this)
  }

  componentDidMount(){

  }

  nextStep(n){
    this.setState({steps: n})
  }

  skip(){
    this.setState({
      steps: this.state.stepsContent.length
    })
  }

  render(){
    return (
      <Overlay step={this.state.steps}>
        {console.log(this.state.steps)}
        {
          this.state.steps === 0 ? (
            <LogoContainer start="true">
              <Animate Animation={[ZoomIn, Shake]}
                       duration={["0.3s","0.3s"]}
                       delay={["0.3s","0.6s"]}
              >
                <Logo logo
                      start="true"
                      onClick={e => this.nextStep(1)}
                />
              </Animate>
            </LogoContainer>
          ) : (
            <LogoContainer>
              <Logo logo />
            </LogoContainer>
          )
        }
        <div>
          {
            this.state.stepsContent.map(item => {
              return item.step === this.state.steps ? item.content : null;
            })
          }
        </div>
      </Overlay>
    );
  }

}

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  ${props => props.step !== 0 ? `z-index: 1300;`:`z-index: 10001;`};
  background-color: ${Const.Color.BACKGROUND.LPINK};
`;

const LogoContainer = styled.div`
  position: fixed;
  ${props => props.start ? `
    width: 291px;
    height: 218px;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
  ` : `
    width: 143px;
    height: 107px;
    top: 22px;
    left: 22px;
  `};
`;

const Logo = styled(Icon)`
  width: ${props => props.start ? `291px` : `143px`};
  height: ${props => props.start ? `218px` : `107px`};
  display: block;
  cursor: pointer;
  &:hover {
    opacity: 0.7;
  }
`;

const Balloon = styled(BalloonComponent)`
  position: absolute;
  top: 50%;
  left: calc(50% - 20vw);
  transform: translate(-50%, -50%);
  max-width: 540px;
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.MIDDLE}px;
  line-height: 1.4em;
  color: ${Const.Color.TEXT.PRIMARY};
  ${props => props.padding ? `padding: ${props.padding};` : `padding: 20px;`}
`;

const NextButton = Button.extend`
  position: absolute;
  bottom: 17px;
  right: 17px;
`;
const ArrowIcon = styled(Icon)`
  width: 9px;
  height: 14px;
  position: absolute;
  top: 50%;
  left: calc(50% + 2px);
  transform: translate(-50%, -50%) rotate(180deg);
`;

const Strong = styled.span`
  color: ${Const.Color.TEXT.SECONDARY};
`;

const Image = styled(ImageComponent)`
  padding: 0 0 20px;
`;

export default Splash;