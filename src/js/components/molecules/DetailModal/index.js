import React from 'react'
import styled from 'styled-components'
import Const from '../../../const'
import getQuery from '../../../components/util/getQuery'

import Button from '../../../components/atoms/Button'
import {H2, Label} from '../../../components/atoms/Text'
import Icon from '../../../components/atoms/Icon'
import CountUp from 'react-countup'

const Modal = ({...props}) => {

  const {match, history, apps} = props;

  const back = e => {
    e.stopPropagation();
    history.goBack();
  };

  const renderData = apps.masterData ? apps.masterData[parseInt(props.id) - 1] : null;

  return (
    <Overlay {...props}
             onClick={e => back(e)}>
      {console.log(renderData)}
      <ModalContainer>
        {/*{*/}
          {/*getQuery().key ? (*/}
            {/*<ArrowButton prev*/}
                         {/*onClick={e => getArrowDestination('prev', props)}*/}
            {/*>*/}
              {/*<ArrowIcon arrow_white />*/}
            {/*</ArrowButton>*/}
          {/*) : null*/}
        {/*}*/}
        <Inner>
          <DetailImage poster={
            renderData !== undefined && !renderData.adult ? renderData.product_image : '/assets/images/noimage_1x1.png'
          }
          >
            {
              renderData !== undefined && renderData.product_link ? (
                <AmazonLink href={renderData !== undefined ? renderData.product_link : ''} target={'_blank'}>
                  <AmazonIcon amazon_white />
                </AmazonLink>
              ) : null
            }
          </DetailImage>
          <ProductInfo>
            <ProductName>{renderData !== undefined ? renderData.product_name : ''}</ProductName>
            <MakerName>{renderData !== undefined ? renderData.maker_name : ''}</MakerName>
          </ProductInfo>
          <SellContainer>
            <PriceContainer>
              <PriceTitle>買取価格</PriceTitle>
              <ProductPrice>
                {
                  renderData !== undefined && renderData.exercise_price !== '0' ? (
                    <CountUp end={parseInt(renderData.exercise_price)}
                             duration={0.4}
                             separator=","
                             redraw={true}
                    />
                  ) : (
                    <span>-</span>
                  )
                }
              </ProductPrice>
            </PriceContainer>
            {
              // FIXME: 消さずに、他の時も使えるように処理分岐させる
              getQuery().key ? (
                <SellCartButton width={`260px`}
                                height={`69px`}
                                bgColor={Const.Color.BACKGROUND.LPINK}
                                rounded
                                onClick={e => {
                                  props.addMyCart(getQuery().key)
                                }}
                >
                  <SellCartIcon cart_white />
                  うるカートに入れる
                </SellCartButton>
              ) : null
            }
          </SellContainer>
          <CloseButton onClick={e => back(e)}>
            <CloseIcon close />
          </CloseButton>
        </Inner>
        {/*{*/}
          {/*getQuery().key ? (*/}
            {/*<ArrowButton next*/}
                         {/*onClick={e => getArrowDestination('next', props)}*/}
            {/*>*/}
              {/*<ArrowIcon arrow_white />*/}
            {/*</ArrowButton>*/}
          {/*) : null*/}
        {/*}*/}
      </ModalContainer>
    </Overlay>
  );
};

const getArrowDestination = (direction, {...props}) => {
  // Prev, Nextの時
  // keyがあるかないか
  if (getQuery().key) {
    const key = parseInt(getQuery().key);
    switch (direction) {
      case 'prev':
        return props.history.push(`/details/${
          (key - 1) < 0 ?
            props.collection[props.collection.length - 1].product_id :
            props.collection[key - 1].product_id
        }?key=${ (key - 1) < 0 ? props.collection.length - 1 : key - 1}`);
      case 'next':
        // console.log(props.collection[key+1])
        // break;
        return props.history.push(`/details/${
          (key + 1) < props.collection.length ? props.collection[key + 1].product_id : props.collection[0].product_id
        }?key=${ (key + 1) < props.collection.length ? key + 1 : 0 }`);
      default:
        return '';
    }
  } else {
    const id = props.render ? parseInt(props.render.product_id) : 1;
    switch (direction) {
      case 'prev':
        return props.history.push(`/details/${
          (id - 1) > 0 ? id - 1 : props.master.length
        }`);
      case 'next':
        return props.history.push(`/mycollection/${
          (id + 1) < props.master.length + 1 ? id + 1 : 1
        }`);
      default:
        return '';
    }
  }
}

const Overlay = styled.div`
  // position: fixed;
  // top: 0;
  // left: 0;
  // width: 100%;
  // height: 100%;
  // overflow-y: scroll;
  // z-index: 180;
`;

const ModalContainer = styled.div`
  position: absolute;
  top: 50%
  left: calc(50% - 425px / 2);
  width: 695px;
  margin: 0 20px;
  height: auto;
  transform: translate(-50%, -50%);
  box-shadow: 0 6px 8px ${Const.Color.SHADOW.A73BLACK}
`;

const Inner = styled.div`
  position: relative;
  width: 100%;
  padding: 26px;
  height: 100%;
  background-color: ${Const.Color.BACKGROUND.A93WHITE};
  border-radius: 5px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

const DetailImage = styled.div`
  position: relative;
  ${props => {return props.poster && props.poster.length !== 0 ?
    `background-image: url(${props.poster});
     background-color: ${Const.Color.BACKGROUND.WHITE};` :
    `background-image: url("/assets/images/noimage_1x1.png");`;
  }}
  width: 326px;
  height: 326px;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: top center;
  
`;

const CloseButton = Button.extend`
  position: absolute;
  width: 13px;
  height: 13px;
  top: 26px;
  right: 26px;
`;

const CloseIcon = styled(Icon)`
  width: 13px;
  height: 13px;
`;

const AmazonLink = styled.a`
  display: flex;
  position: absolute;
  bottom: 10px;
  right: 10px;
  width: 47px;
  height: 47px;
  border-radius: 50%;
  background-color: ${Const.Color.BACKGROUND.AMAZON_ORANGE};
  justify-content: center;
  align-items: center;
`;

const AmazonIcon = styled(Icon)`
  width: 29px;
  height: 27px;
`;

const ProductInfo = styled.div`
  width: 50%;
  padding: 0 30px 0 26px;
`;

const ProductName = H2.extend`
  color: ${Const.Color.TEXT.PRIMARY};
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
`;

const MakerName = Label.extend`
  margin: 8px 0 0;
  color: ${Const.Color.TEXT.WEAK};
  font-size: ${Const.Size.FONT.BASE}px;
`;

const ArrowButton = Button.extend`
  position: absolute;
  width: 38px;
  height: 38px;
  background-color: ${Const.Color.BACKGROUND.A80BLACK};
  border-radius: 50%;
  z-index: 10;
  top: 50%;
  cursor: pointer;
  ${props => {return props.prev ? 
    `left: -19px;
     transform: translateY(-50%) rotate(0);
    ` :
    `right: -19px;
     transform: translateY(-50%) rotate(180deg);
    `
  }}
`;

const ArrowIcon = styled(Icon)`
  position: absolute;
  width: 12px;
  height: 20px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const SellContainer = styled.div`
  width: 260px;
  position: absolute;
  bottom: 26px;
  right: 26px;
`;

const SellCartButton = Button.extend`
  position: relative;
  color: ${Const.Color.TEXT.WHITE};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  font-family: "waon_joyo", serif;
  padding: 0 0 0 25px;
  border: 6px solid ${Const.Color.BORDER.WHITE}
`;

const SellCartIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 18px;
  width: 31px;
  height: 25px;
  transform: translateY(-50%);
`;

const ProductPrice = Label.extend`
  font-size: ${Const.Size.FONT.HIGH_LARGE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.SECONDARY};
  &:before {
    content: "¥";
    font-size: ${Const.Size.FONT.MIDDLE}px;
    color: ${Const.Color.TEXT.SECONDARY};
  }
`;

const PriceContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  padding: 0 0 10px;
`;

const PriceTitle = styled.span`
  display: block;
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  color: ${Const.Color.TEXT.SECONDARY};
`;

export default Modal;