import React from 'react'
import styled from 'styled-components'
import Const from '../../../const'

import Icon from '../../atoms/Icon'
import Image from '../../atoms/Image'
import {H2, Label} from '../../atoms/Text'
import Button from '../../atoms/Button'
import CountUp from 'react-countup'

const CategoryCard = ({...props}) => {
  return (
    <Container>
      <ProductContainer poster={props.poster}>
        <ProductInner>
          <ProductInfo>
            <ProductInfoTitle>
              {props.name_ja}
              <span>{props.name_en}</span>
            </ProductInfoTitle>
          </ProductInfo>
        </ProductInner>
      </ProductContainer>
    </Container>
  )
}

const Container = styled.div`
  position: relative;
  background-color: ${Const.Color.BACKGROUND.WHITE};
  border-radius: 5px;
  // width: 234px;
  width: 224px;
  height: 339px;
  overflow: hidden;
  margin: 16px 0 0;
  cursor: pointer;
`;

const ProductContainer = styled.div`
  ${props => {return (props.poster.length !==  0 || !props.poster) ?
  `background-image: url(${props.poster});` :
  `background-image: url(/assets/imaegs/noimage.png);`
  }}
  background-size: cover;
  background-repeat: no-repeat;
  background-position: top center;
  height: 100%;
  &:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    background: linear-gradient(to bottom, rgba(0,0,0,0) 0%,rgba(0,0,0,0.8) 100%);
    width: 100%;
    height: 100%;
  }
`;

const ProductInner = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 80px;
  z-index: 10;
`;

const ProductInfo = styled.div`
  padding: 10px 15px 0;
  text-align: left;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

const ProductInfoTitle = Label.extend`
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  font-weight: bold;
  line-height: 1.2em;
  color: ${Const.Color.TEXT.WHITE};
  span {
    display: block;
    margin: 8px 0 0;
    font-size: ${Const.Size.FONT.SMALL}px;
  }
`;

export default CategoryCard;