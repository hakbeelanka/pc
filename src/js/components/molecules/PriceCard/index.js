import React from 'react'
import styled from 'styled-components'
import Const from '../../../const'
import {
  map,
  filter
} from 'lodash'
import {Link} from 'react-router-dom'

import Icon from '../../atoms/Icon'
import {H2, Label} from '../../atoms/Text'
import Button from '../../atoms/Button'
import CountUp from 'react-countup'
import Modal from '../../molecules/DetailModal'

const PriceCard = ({...props}) => {
  const { match,apps } = props;
  return (
    <Container>
      <Link to={{
        pathname: `/details/${props.data.product_id}`,
        state: {modal: true}
      }} >
        <ProductContainer poster={props.poster}>
            <ProductInner>
              <ProductInfo>
                <ProductInfoText>{props.description}</ProductInfoText>
              </ProductInfo>
              <ProductPriceWrapper>
                <ExtendLabel>買取価格</ExtendLabel>
                <ProductPrice>
                  {
                    props.price !== '0' ? (
                      <CountUp end={parseInt(props.price.replace(',',''))}
                               separator=","
                      />
                    ) : (
                      <span>-</span>
                    )
                  }
                </ProductPrice>
              </ProductPriceWrapper>
            </ProductInner>
        </ProductContainer>
      </Link>
      <FunctionsButton>
        <ExtendButton width="calc(50% - 0.5px)"
                      height="70px"
                      bgColor={Const.Color.BACKGROUND.BLUE}
                      onMouseEnter={e => {return props.bgColor = 'white'}}
                      onClick={e => {
                        const collection_data = Object.assign({}, props.data);
                        collection_data.isSell = false;
                        collection_data.isUnopened = false;
                        props.addMyCollection(collection_data);
                        props.calculateTotalAmount()
                      }}
        >
          <CollectionInIcon plus_white />
          <ButtonLabel>コレクション追加</ButtonLabel>
        </ExtendButton>
        <ExtendButton width="calc(50% - 0.5px)"
                      height="70px"
                      bgColor={Const.Color.BACKGROUND.LPINK}
                      onClick={e => {
                        // TODO: 同じ商品が2つ入っているときは追加しない。条件分岐
                        const collection_data = Object.assign({}, props.data);
                        let count = 0;
                        props.user.collection.length > 0 ?
                        props.user.collection.reduce((p,c) => {
                          // FIXME: 今後はproduct_idベースにする必要がある
                          if (c.jan_code === collection_data.jan_code) { count++; }
                        }): null;
                        if (count > 1) {
                          // TODO: 2件以上カートに入れようとしている処理。

                          collection_data.isSell = false;
                          collection_data.isUnopened = false;
                          props.addMyCollection(collection_data);
                          props.calculateTotalAmount();
                        } else {
                          collection_data.isSell = true;
                          collection_data.isUnopened = false;
                          props.addMyCollection(collection_data);
                          props.calculateTotalAmount();
                        }
                      }}
        >
          <BoxInIcon cart_white />
          <ButtonLabel>うるカートへ追加</ButtonLabel>
        </ExtendButton>
      </FunctionsButton>
    </Container>
  )
}

const Container = styled.div`
  position: relative;
  background-color: ${Const.Color.BACKGROUND.WHITE};
  border-radius: 5px;
  // width: 234px;
  width: 224px;
  height: 339px;
  overflow: hidden;
  margin: 16px 0 0;
`;

const ProductContainer = styled.div`
  ${props => {return (props.poster.length !==  0) ?
  `background-image: url(${props.poster});` :
  `background-image: url(/assets/images/noimage.png);`
  }}
  background-size: cover;
  background-repeat: no-repeat;
  background-position: top center;
  height: 100%;
  cursor: pointer;
  &:hover {
    opacity: 0.7;
  }
`;

const ProductInner = styled.div`
  position: absolute;
  bottom: 70px;
  left: 0;
  width: 100%;
  height: 110px;
  background-color: ${Const.Color.BACKGROUND.A80WHITE};
`;

const ProductInfo = styled.div`
  padding: 10px 10px 0;
  height: 70px;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: flex-start;
`;

const ProductInfoText = Label.extend`
  font-size: ${Const.Size.FONT.SMALL}px;
  line-height: 1.2em;
  color: ${Const.Color.TEXT.DEFAULT};
  cursor: pointer;
  &:hover {
    color: ${Const.Color.TEXT.SECONDARY};
  }
`;

const ProductPriceWrapper = styled.div`
  padding: 0 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const ExtendLabel = Label.extend`
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.SECONDARY};
`;

const ProductPrice = H2.extend`
　font-size: ${Const.Size.FONT.LARGE}px;
  color: ${Const.Color.TEXT.SECONDARY};
  &:before {
    font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
    content: '¥';
  }
`;

const FunctionsButton = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 70px;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  background-color: ${Const.Color.BACKGROUND.WHITE};
`;

const BoxInIcon = styled(Icon)`
  width: 35px;
  height: 38px;
`;

const CollectionInIcon = styled(Icon)`
  width: 21px;
  height: 38px;
`;

const ButtonLabel = Label.extend`
  margin: 8px 0 0;
  font-size: ${Const.Size.FONT.SMALL}px;
  font-family: "waon_joyo", serif;
  color: ${Const.Color.TEXT.WHITE};
`;

const ExtendButton = styled(Button)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  cursor: pointer;
`;

export default PriceCard;