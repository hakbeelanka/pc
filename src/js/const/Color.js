export default {
  PRIMARY: '#5a667a',
  SECONDARY: '#979da5',
  STRENGTH: '#ee4368',
  INTELLIGENCE: '#493aa3',
  DEFAULT: '#ffffff',
  TEXT: {
    DEFAULT: '#000000',
    // PRIMARY: '#4c4c4c',
    PRIMARY: '#37363f',
    SECONDARY: '#ff406d',
    WEAK: '#b6bfce',
    WHITE: '#ffffff'
  },
  BORDER: {
    WHITE: '#ffffff',
    GRAY: '#dfdfdf',
    DGRAY: '#83838b'
  },
  BACKGROUND: {
    BLACK: '#37363f',
    WHITE: '#ffffff',
    GRAY: '#c1c7d0',
    DGRAY: '#6b7278',
    D2GRAY: '#4d4f5a',
    GREEN: '#08b966',
    BLUE: '#00bce4',
    D2BLUE: '#008ec9',
    D3BLUE: '#0576a5',
    DBLUE: '#007eff',
    LBLUE: '#ebf6f8',
    L2BLUE: '#c2f4ff',
    PINK: '#ff554e',
    LPINK: '#ff406d',
    ORANGE: '#ffae00',
    AMAZON_ORANGE: '#f89513',
    YELLOW: '#f4bd00',
    LYELLOW: '#e9fe11',
    A80BLACK: 'rgba(76,76,76,0.8)',
    A80WHITE: 'rgba(255,255,255,0.8)',
    A45WHITE: 'rgba(255,255,255,0.45)',
    A93WHITE: 'rgba(255,255,255,0.93)',
    A93BLUE: 'rgba(228,238,240,0.93)',
    A85RED: 'rgba(235,63,57, 0.85)',
    A85GREEN: 'rgba(0,167,79,0.85)',
    A85YELLOW: 'rgba(220,176,0,0.85)'
  },
  SHADOW: {
    A36BLACK: 'rgba(16,16,16,0.36)',
    A73BLACK: 'rgba(55,55,60,0.73)'
  },
  DANGER: '#XXXXXX'
}