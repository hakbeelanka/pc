export default {
  FONT: {
    SMALL: 12,
    BASE: 16,
    LOW_MIDDLE: 18,
    MIDDLE: 26,
    LARGE: 32,
    HIGH_LARGE: 47
  },
  HEADER_HEIGHT: 70
}