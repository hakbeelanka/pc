import React, {Component} from "react"
import {bindActionCreators} from 'redux'
import {createBrowserHistory} from "history"
import {Route, Switch} from "react-router-dom"
import {connect, Provider} from "react-redux"
import persistState from 'redux-localstorage'
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import TransitionGroup from "react-transition-group/TransitionGroup"
import AnimatedSwitch from "./components/util/animated_switch"
import EventListener from 'react-event-listener'
import styled from 'styled-components'
import {ConnectedRouter, routerMiddleware, routerReducer,} from "react-router-redux"
import createSagaMiddleware from 'redux-saga'
import { ModalContainer, ModalRoute } from 'react-router-modal'
import {composeWithDevTools} from 'redux-devtools-extension'
import userReducer, {
  addMyCart,
  welcomeApps
} from './redux/modules/user'
import appsReducer, {
  changeWindowWidth,
  changeWindowHeight,
  requestFetchCollection,
  requestFetchCategory,
  requestFetchHighPrice,
  setTipsPosition,
  openTips
} from './redux/modules/apps'
import rootSaga from './redux/middleware/saga'
import Const from './const'
import ReaderContainer from "./components/atoms/Reader"
import HeaderContainer from "./containers/organisms/HeaderContainer"
import FunctionContainer from "./containers/organisms/FunctionContainer"
import SideMenuContainer from "./containers/organisms/SideMenu"
import CharacterContainer from './components/molecules/CharacterContainer'
import patternBlue from './containers/pages/background/pattern_blue.png'

import Home from "./containers/pages/Home"
import HighPrice from "./containers/pages/HighPrice"
import MyCollection from "./containers/pages/MyCollection"
import MyCart from "./containers/pages/MyCart"
import Categories from "./containers/pages/Categories"
import SearchResult from "./containers/pages/SearchResult"
import Campaign from "./containers/pages/Campaign"
import Flow from "./containers/pages/Flow"
import Modal from "./components/molecules/DetailModal"
import Missed from "./containers/pages/404"
import Tips from "./components/molecules/Tips";
import Splash from "./components/molecules/Splash";
// TODO: 高価買取の取得処理もするべき？

const APP_VERSION = '1.0.0';

const Header = styled(HeaderContainer)``;
const Functions = styled(FunctionContainer)``;

const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const enhancer = compose(persistState(["user"]))

const store = createStore(
  combineReducers({
    apps: appsReducer,
    user: userReducer,
    routing: routerReducer
  }),
  composeWithDevTools(applyMiddleware(
    routerMiddleware(history),
    sagaMiddleware
  )),
  enhancer
);

sagaMiddleware.run(rootSaga);

const ExtendModal = (props, id) => {
  console.log(props, id)
  const back = e => {
    e.stopPropagation();
    props.history.goBack();
  };
  return (
    <ModaiOverray onClick={e => back(e)}>
      <Modal {...id} {...props}/>
    </ModaiOverray>
  )
}

class RootContainer extends Component {

  previousLocation = this.props.location;

  componentWillMount(){
    this.props.requestFetchCategory();
    this.props.requestFetchHighPrice();
    this.props.requestFetchCollection();

    // アプリのバージョンを更新する。
    // マスタデータの更新内容と自分のデータを比較する。

  }

  componentWillUpdate(nextProps) {
    const { location } = this.props;
    // set previousLocation if props.location is not modal
    if (
      nextProps.action !== "POP" &&
      (!location.state || !location.state.modal)
    ) {
      this.previousLocation = this.props.location;
    }
  }

  handleResize = () => {
    this.props.changeWindowWidth(window.innerWidth);
    this.props.changeWindowHeight(window.innerHeight);
  };

  render() {
    const props = this.props;
    const isModal = !!(
      props.location.state &&
      props.location.state.modal
    ); // not initial render

    return(
      <AppContainer location={props.location}>
        <EventListener target="window" onResize={this.handleResize}/>
        <Header/>
        <TransitionGroup component="main" className="main">
          {/*<SideMenuContainer />*/}
          <div className={`animated-page-wrapper`}>
            <Switch
              key={props.location.key}
              location={isModal ? this.previousLocation : props.location}
            >
              <Route exact
                     path="/"
                     render={props => (
                       <Home {...props} />
                     )}
              />
              <Route path="/highprice/"
                     render={props => (
                       <HighPrice {...props} />
                     )}
              />
              <Route path="/campaign/"
                     render={props => (
                       <Campaign {...props} />
                     )}
              />
              <Route path="/flow/"
                     render={props => (
                       <Flow {...props} />
                     )}
              />
              <Route path="/mycollection/"
                     render={props => (
                       <MyCollection {...props} />
                     )}
              />
              <Route path="/mycart/"
                     render={props => (
                       <MyCart {...props} />
                     )}
              />
              <Route path="/categories/:id"
                     render={props => (
                       <Categories {...props} />
                     )}
              />
              <Route path="/results/:id"
                     render={props => (
                       <SearchResult {...props} />
                     )}
              />
              <Route path="/details/:id"
                     render={props => ExtendModal({...props, ...this.props}, props.match.params)}
              />
              <Route component={Missed}/>
            </Switch>
            {isModal ?
              <Route path="/details/:id"
                     render={props => ExtendModal({...props, ...this.props}, props.match.params)}
              /> : null}
          </div>
          <Character {...this.props}>
            <CharacterContainer {...this.props}
                                {...props}
            />
          </Character>
        </TransitionGroup>
        <SideMenuContainer />
        <Functions/>
        {
          !this.props.user.isUser ? (
            <Splash {...this.props} />
          ) : null
        }
        {
          this.props.apps.isOpenTips && props.location.pathname === "/" ? (
            <Tips {...this.props} />
          ) : null
        }
      </AppContainer>
    )
  }
}

const Character = styled.div`
  position: absolute;
  top: 40px;
  right: -80px;
  ${props => { return (history.location.pathname.indexOf('/details') !== -1 && history.location.pathname.replace('/details', '').length > 0) ? 
    `z-index: 100;` : `z-index: 100;`
  }}
  ${props => { return (history.location.pathname.indexOf('/mycart') !== -1) ?
    `display: none;` : `display: block;`
  }}
  ${props => { return !props.user.isUser ? `z-index: 10000;` : ``}}
`;

const Reader = styled(ReaderContainer)`
  position: fixed;
  top: 6px;
  left: 4px;
  z-index: 100;
`;

const QRContainer = styled.div`
  position: relative;
  width: 100%;
  padding: 20px 0 0;
`;

const AppContainer = styled.div`
  width: 100%;
  height: 100%;
  ${props => {return getBackground(props)}}
  background-repeat: repeat;
  transition: background 1500ms;
  &:after {
    ${() => { return (history.location.pathname.indexOf('/mycollection') !== -1 && history.location.pathname.replace('/mycollection', '').length > 0) ?
       "display: block;" : "display:none;"
  }}
    content: '';
    position: fixed;
    width: 100%;
    height: 100%;
    background-color: ${Const.Color.BACKGROUND.A80BLACK};
    top: 0;
    left: 0;
    z-index: 170;
  }
`;

const ModaiOverray = styled.div`
    position: fixed;
    background: rgba(0,0,0,0.5);
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    z-index: 10000;
`;

const getBackground = props => {
  switch (props.location.pathname) {
    case '/':
      return `background-image: url(${patternBlue});`;
    case '/highprice':
      return `background-image: url(${patternBlue});`;
    case '/mycollection':
      return `background-color: ${Const.Color.BACKGROUND.L2BLUE};`;
    case '/categories/titles':
      return `background-color: ${Const.Color.BACKGROUND.PINK};`;
    case '/categories/series':
      return `background-color: ${Const.Color.BACKGROUND.GREEN};`;
    case '/categories/makers':
      return `background-color: ${Const.Color.BACKGROUND.YELLOW};`;
    default :
      return `background-color: ${Const.Color.BACKGROUND.L2BLUE};`;
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    apps: state.apps
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    changeWindowWidth,
    changeWindowHeight,
    requestFetchCollection,
    requestFetchCategory,
    requestFetchHighPrice,
    addMyCart,
    welcomeApps,
    setTipsPosition,
    openTips
  }, dispatch)
}

const ConnectedRootContainer = connect(mapStateToProps, mapDispatchToProps)(RootContainer);

export default () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route render={props=>(
        <ConnectedRootContainer {...props} />
      )}/>
    </ConnectedRouter>
  </Provider>
)