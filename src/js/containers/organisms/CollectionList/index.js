import React from 'react'
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled,{extend} from 'styled-components'
import { withRouter } from 'react-router-dom'
import { pure } from 'recompose'
import {
  addMyCart,
  removeMyCart,
  removeMyCollection,
  calculateTotalAmount
} from "../../../redux/modules/user";

import CountUp from 'react-countup'
import Const from "../../../const/"
import {H2} from '../../../components/atoms/Text'
import ProductCard from '../../../components/molecules/ProductCard'
const CollectionList = pure(props => {

  // TODO: 複数所持の場合を想定していない。

  return (
    <Container>
      <TitleContainer>
        <Title>あなたのコレクション一覧</Title>
        <TotalAmountContainer>
          <TotalAmount>
            <CountUp end={parseInt(props.user.total_amount)}
                     separator=","
                     duration={0.01}
            />
          </TotalAmount>
        </TotalAmountContainer>
      </TitleContainer>
      <Inner>
        {
          props.user.collection.map((item, index) => {
            return (
              <ProductCard
                {...props}
                {...item}
                key={`hpc-${index}`}
                up={false}
                selectKey={index}
              />
            )
          })
        }
      </Inner>
    </Container>
  )
});

const mapStateToProps = state => {
  return {
    user: state.user,
    apps: state.apps
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    addMyCart,
    removeMyCart,
    removeMyCollection,
    calculateTotalAmount
  }, dispatch)
}

const Container = styled.div`
  position: relative;
  border-radius: 0 0 5px 5px;
  padding: 20px 20px 160px;
`;

const Inner = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;
  background-color: ${Const.Color.BACKGROUND.WHITE};
  border: 1px solid ${Const.Color.BORDER.DGRAY};
`;

const Title = H2.extend`
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.MIDDLE}px;
  color: ${Const.Color.TEXT.PRIMARY};
`;

const TotalAmount = H2.extend`
  display: inline-block;
  font-size: ${Const.Size.FONT.LARGE}px;
  color: ${Const.Color.TEXT.SECONDARY};
  &:before {
    display: inline-block;
    content: '¥';
    font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
    color: ${Const.Color.TEXT.SECONDARY};
    margin: 0 1px 0 0;
  }
`;

const TotalAmountContainer = styled.div`
  width: auto;
  &:before {
    display: inline-block;
    content: '総額';
    font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
    color: ${Const.Color.TEXT.PRIMARY};
    margin: 0 8px 0 0;
  }
`;

const TitleContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: 0 0 15px;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionList));