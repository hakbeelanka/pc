import React, {Component} from 'react'
import styled,{css} from 'styled-components'
import Const from '../../../const/index'
import {bindActionCreators} from 'redux'
import Icon from '../../../components/atoms/Icon/index'
import {connect} from "react-redux";
import {withRouter} from "react-router-dom"
import flattenDeep from 'lodash/flattenDeep'
import filter from 'lodash/filter'
import intersectionWith from 'lodash/intersectionWith'
import isEqual from 'lodash/isEqual'
import InputText from '../../../components/atoms/InputText'
import {
  requestFetchCollection,
  changeSuggestions,
  changeSearchKeyword
} from "../../../redux/modules/apps";

class KeywordSearchForm extends Component{

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      suggestions: []
    }
  }

  getAndSearch = value => {
    // FIXME: 名前全文検索でmatchするものを拾って来る必要がある。
    // FIXME: AND検索ができるように
    const inputValue = value.trim().toLowerCase().replace(/　/g, " ").split(' ');
    const masterdata = Array.isArray(this.props.apps.masterData) ?
      this.props.apps.masterData.map(item => {
        return JSON.stringify(item);
      }) : [];
    let filterData = inputValue.map((item, index) => {
      return filter(masterdata, o => {
        return o.indexOf(item) !== -1;
      })
    }).reduce((a, c) => {
      return intersectionWith(a, c, isEqual)
    }).map(v => {
      return JSON.parse(v);
    });

    return filterData.length > 500 ? [] : filterData;
  };

  render(){
    return (
      <Container>
        <SearchInput innerRef={x => this.input = x}
                     placeholder={`キーワードから検索`}
                     onMouseEnter={() => this.input.focus()}
        />
        <SearchButton onClick={e => {
          this.props.changeSearchKeyword(this.input.value)
          this.props.changeSuggestions(this.getAndSearch(this.input.value))
          this.props.history.push(`/results/keywords`)
        }}>
          <SearchIcon search_blue />
        </SearchButton>
      </Container>
    )
  }
}

const Container = styled.div`
  position: relative; 
  width: 366px;
  height: 95px;
  border-radius: 95px;
  background-color: ${Const.Color.BACKGROUND.WHITE};
  box-shadow: 0 3px 0 ${Const.Color.SHADOW.A36BLACK};
`;

const SearchButton = styled.button`
  position: absolute;
  top: 0;
  right: 0;
  width: 95px;
  height: 95px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  cursor: pointer;
`;

const SearchIcon = styled(Icon)`
  display: block; 
  width: 24px;
  height: 25px;
  font-size: ${Const.Size.FONT.BASE}px;
  font-weight: normal;
  color: ${Const.Color.TEXT.BLACK};
`;

const AutosuggestStyle = css` 
  .react-autosuggest__container {
    position: relative;
  }
  .react-autosuggest__input {
    position: absolute;
    top: 0;
    left: 0;
    width: calc(100% - 95px);
    height: 95px;
    border-radius: 95px 0 0 95px;
    border: none;
    padding: 30px 0px 30px 42px;
  }
  .react-autosuggest__input:focus {
    outline: none;
  }

`;

const SearchInput = styled(InputText)`
  position: absolute;
  top: 0;
  left: 0;
  width: calc(100% - 95px);
  height: 95px;
  border-radius: 95px 0 0 95px;
  border: none;
  padding: 30px 0px 30px 42px;
`;


const mapStateToProps = state => {
  return {
    user: state.user,
    apps: state.apps
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    requestFetchCollection,
    changeSuggestions,
    changeSearchKeyword
  }, dispatch)
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(KeywordSearchForm));