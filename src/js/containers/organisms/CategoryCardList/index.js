import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import styled, {extend} from 'styled-components'
import {withRouter} from 'react-router-dom'
import {pure} from 'recompose'
import Animate, {Tada} from 'animate-css-styled-components'
import isArray from '../../../components/util/isArray'
import Const from "../../../const/"
import CategoryCard from "../../../components/molecules/CategoryCard"
import {Label} from '../../../components/atoms/Text'
import {
  changeSearchKeyword
} from "../../../redux/modules/apps";


const CategoryCardList = pure(props => {

  let {match, apps} = props;

  const category = apps.categoriesData[match.params.id];
  return (
    <Container>
      <Inner>
        {
          isArray(category) ? (
            category.map((item, index) => {
              return (
                <div key={`hpc-${index}`}
                     onClick={e => {
                       props.changeSearchKeyword(item.search_key[0]);
                       props.history.push(`/results/${match.params.id}/?cat=${item.link}`)
                     }}
                >
                  {/*<ContainerAnimate Animation={[Tada]}*/}
                  {/*duration={["0.7s"]}*/}
                  {/*delay={[`${1+0.2*index}s`]}*/}
                  {/*>*/}
                  <CategoryCard {...item}/>
                  {/*</ContainerAnimate>*/}
                </div>
              )
            })
          ) : []
        }
      </Inner>
    </Container>
  )
});

const mapStateToProps = state => {
  return {
    apps: state.apps
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    changeSearchKeyword
  }, dispatch)
}

const Container = styled.div`
  position: relative;
  padding: 20px 20px 140px;
  border-radius: 0 0 5px 5px;
`;

const Inner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: wrap;
  margin: -16px 0 0;
  > div:nth-child(3n+2):last-child {
    margin-right: 34%;
  }
`;

const TitleContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  text-align: center;
`;

const Message = Label.extend`
  font-size: ${Const.Size.FONT.MIDDLE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.WHITE};
  span {
    font-size: ${Const.Size.FONT.LOW_MIDDLE}px;  
  }
`;

const ContainerAnimate = styled(Animate)`
  // opacity: 0;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryCardList));