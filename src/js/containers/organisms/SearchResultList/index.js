import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import styled, {extend} from 'styled-components'
import {withRouter} from 'react-router-dom'
import {pure} from 'recompose'
import Animate, {Tada} from 'animate-css-styled-components';
import getQuery from '../../../components/util/getQuery'
import isArray from '../../../components/util/isArray'
import {
  filter,
  without
} from 'lodash'
import {
  addMyCollection,
  calculateTotalAmount
} from "../../../redux/modules/user"

import Const from "../../../const/"
import PriceCard from "../../../components/molecules/PriceCard"
import {Label} from '../../../components/atoms/Text'
import intersectionWith from "lodash/intersectionWith";
import isEqual from "lodash/isEqual";


const SearchResultList = pure(props => {

  const resultsData = (()=>{
    if (!isArray(props.apps.masterData)) return [];
    if (props.apps.categoriesData.length === 0) return [];
    let key = [];
    let ret = [];
    const masterdata = isArray(props.apps.masterData) ?
      props.apps.masterData.map(item => {
        return JSON.stringify(item);
      }) : [];
    switch (props.slug) {
      case 'keywords':
        return props.apps.suggestions;
      case 'titles':
        // クエリで検索して、返却された配列を用意する
        key = props.apps.categoriesData[props.slug].map((item, index) => {
          return (item.link === getQuery().cat) ? item.search_key : null}).filter(key=>{return key !== null;})
        if (key.length === 0) break;
        ret = key.map((item, index) => {
          return filter(masterdata, o => {
            return o.indexOf(item) !== -1;
          })
        }).reduce((a, c) => {
          return intersectionWith(a, c, isEqual)
        }).map(v => {
          return JSON.parse(v);
        });
        return ret;
      case 'makers':
        // クエリで検索して、返却された配列を用意する
        key = props.apps.categoriesData[props.slug].map((item, index) => {
          return (item.link === getQuery().cat) ? item.search_key : null}).filter(key=>{return key !== null;})
        if (key.length === 0) break;
        ret = key.map((item, index) => {
          return filter(masterdata, o => {
            return o.indexOf(item) !== -1;
          })
        }).reduce((a, c) => {
          return intersectionWith(a, c, isEqual)
        }).map(v => {
          return JSON.parse(v);
        });
        return ret;
      case 'series':
        // クエリで検索して、返却された配列を用意する
        key = props.apps.categoriesData[props.slug].map((item, index) => {
          return (item.link === getQuery().cat) ? item.search_key : null}).filter(key=>{return key !== null;})
        if (key.length === 0) break;

        ret = key.map((item, index) => {
          return filter(masterdata, o => {
            return o.indexOf(item) !== -1;
          })
        }).reduce((a, c) => {
          return intersectionWith(a, c, isEqual)
        }).map(v => {
          return JSON.parse(v);
        });
        return ret;
      default:
        return props.apps.suggestions;
    }
  })()

  return (
    <Container>
      <TitleContainer>
        <Message>検索結果</Message>
        {
          props.apps.lastSeachText.length !== 0 ? (
            <Keyword>{props.apps.lastSeachText}</Keyword>
          ) : null
        }
      </TitleContainer>
      {
        isArray(resultsData) ? (
          <Inner>
            {
              resultsData.length !== 0 ?
                resultsData.map((item, index) => {
                  return (
                    <div key={`src-${index}`}>
                      <ContainerAnimate Animation={[Tada]}
                                        duration={["0.7s"]}
                                        delay={[`${1+0.2*index}s`]}
                      >
                        <PriceCard
                          {...props}
                          data={{...item}}
                          poster={!item.adult ? item.product_image : ""}
                          price={item.exercise_price}
                          description={item.product_name}
                        />
                      </ContainerAnimate>
                    </div>
                  )
                }) : (
                  <NoResults>キーワードに該当する商品はありませんでした。</NoResults>
                )
            }
          </Inner>
        ) : null
      }
    </Container>
  )
});

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    addMyCollection,
    calculateTotalAmount
  },dispatch)
}

const Container = styled.div`
  position: relative;
  padding: 20px 20px 140px;
  border-radius: 0 0 5px 5px;
`;

const Inner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: wrap;
  > div:nth-child(3n+2):last-child {
    margin-right: 34%;
  }
`;

const TitleContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  text-align: left;
`;

const Message = Label.extend`
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.MIDDLE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.PRIMARY};
`;

const Keyword = Label.extend`
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.WHITE};
  display: inline-flex;
  justify-content: center;
  align-items: center;
  background-color: ${Const.Color.BACKGROUND.D2GRAY};
  height: 36px;
  padding: 0 12px;
  border-radius: 36px;
  margin: 0 0 0 12px;
`;

const ContainerAnimate = styled(Animate)`
  // opacity: 0;
`;

const NoResults = styled.div`
  margin: 20px 0 0;
`;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResultList);