import React,{Component} from 'react'
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { withRouter, NavLink } from 'react-router-dom'
import { pure } from 'recompose'
import {
  openBarcodeSearch,
  closeBarcodeSearch,
  openMyCart,
  closeMyCart,
  setTipsPosition
} from "../../../redux/modules/apps";
import {
  removeMyCart,
  checkUnopenedCollection,
  uncheckUnopenedCollection
} from "../../../redux/modules/user";
import _filter from 'lodash/filter';

import Const from "../../../const/"
import Icon from "../../../components/atoms/Icon"
import Button from "../../../components/atoms/Button"
import ApplyButton from "../../../components/molecules/ApplyButton"
import KeywordSearchForm from "../KeywordSearchForm"
import BadgeComponent from "../../../components/atoms/Badge"
import {Label} from "../../../components/atoms/Text"
import BigBalloon from "../../../components/molecules/BigBalloon"
import LandscapeCard from "../../../components/molecules/LandscapeCard"

class FunctionContainer extends Component {

  constructor(props) {
    super(props);
    this.state={}
    this.keywordsearch = React.createRef();
    this.collectionbutton = React.createRef();
    this.applybutton = React.createRef();
  }

  componentDidMount(){
    this.props.setTipsPosition('keywordsearch', [this.keywordsearch.current.offsetLeft,this.keywordsearch.current.offsetTop])
    this.props.setTipsPosition('applybutton', [this.applybutton.current.offsetLeft,this.applybutton.current.offsetTop])
    this.props.setTipsPosition('collectionbutton', [this.collectionbutton.current.offsetLeft,this.collectionbutton.current.offsetTop])
  }

  render(){
    return (
      <Container>
        <Inner>
          {/*<ExtendButton circle*/}
          {/*width="95px"*/}
          {/*height="95px"*/}
          {/*bgColor={Const.Color.BACKGROUND.BLUE}*/}
          {/*onClick={e => {*/}
          {/*if (!props.apps.isOpenBarcodeSearch){*/}
          {/*props.openBarcodeSearch();*/}
          {/*} else {*/}
          {/*props.closeBarcodeSearch();*/}
          {/*}*/}
          {/*}}*/}
          {/*>*/}
          {/*<BarCodeIcon barcode />*/}
          {/*<ButtonLabel>バーコード<br />検索</ButtonLabel>*/}
          {/*</ExtendButton>*/}
          {
            !this.props.apps.isOpenBarcodeSearch ?
              null :
              (
                <BarcodeReaderContainer >
                  <BigBalloon isShadow
                              left={40}
                  >
                    <h1>バーコード検索</h1>
                  </BigBalloon>
                </BarcodeReaderContainer>
              )
          }
          <div ref={this.keywordsearch}>
            <KeywordSearch />
          </div>
          <div ref={this.collectionbutton}>
            <ExtendButton circle
                          width="95px"
                          height="95px"
                          bgColor={Const.Color.BACKGROUND.BLUE}
                          innerRef={`toCollectionButton`}
            >
              <ExtendNavLink exact to={"/mycollection"}>
                <CollectionIcon collection_white />
                <ButtonLabel>あなたの<br />コレクション</ButtonLabel>
                <Badge value={this.props.user.collection.length} />
              </ExtendNavLink>
            </ExtendButton>
          </div>
          <ApplyCartContainer onMouseEnter={e => {
            if (_filter(this.props.user.collection,(o)=>{ return o.isSell }).length > 0){
              this.props.openMyCart()
            }
          }}
                              onMouseLeave={e => this.props.closeMyCart()}
          >
            <div ref={this.applybutton}>
              <ApplyButton {...this.props}/>
            </div>
            {
              !this.props.apps.isOpenMyCart ?
                null :
                (
                  <div>
                    {
                      _filter(this.props.user.collection,(o)=>{ return o.isSell }).length !== 0 ? (
                        <MyCartContainer height={_filter(this.props.user.collection,(o)=>{ return o.isSell }).length * 130 + 20 }>
                          <BigBalloon isShadow
                                      left={190}
                          >
                            {
                              _filter(this.props.user.collection,(o)=>{ return o.isSell }).map((item, index) => {
                                return (
                                  <ExtendLandscapeCard key={`lnd-${index}`}
                                                       selectKey={index}
                                                       {...this.props}
                                                       {...item}
                                  />
                                )
                              })
                            }
                          </BigBalloon>
                        </MyCartContainer>
                      ) : null
                    }
                  </div>
                )
            }
          </ApplyCartContainer>
          <OldnijiouButton isShadow
                           width={`103px`}
                           height={`103px`}
                           circle
                           bgColor={`#ff6085`}
                           onClick={e => window.location = `https://nijigen-okoku.jp/`}
          >
            にじおう！<br />本サイトへ
          </OldnijiouButton>
        </Inner>
      </Container>
    )
  }
}

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    openBarcodeSearch,
    closeBarcodeSearch,
    openMyCart,
    closeMyCart,
    removeMyCart,
    checkUnopenedCollection,
    uncheckUnopenedCollection,
    setTipsPosition
  }, dispatch)
}

const Container = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  z-index: 1000;
  width: 100%;
  height: 117px;
  border-bottom: 1px solid ${Const.Color.BORDER.GRAY};
  background-color: ${Const.Color.BACKGROUND.A80BLACK};
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

const Inner = styled.div`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 1020px;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const ExtendButton = styled(Button)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const ExtendNavLink = styled(NavLink)`
  position: absolute;
  top: 0;
  left: 0;
  text-decoration: none;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border-radius: 50%;
  // FIXME: 本当はどうあるべき？
  &.active {
    background-color: ${Const.Color.BACKGROUND.ORANGE};
  }
`;

const BarcodeReaderContainer = styled.div`
  // FIXME: 入っている要素の高さで絶対位置が決まるように修正
  position: absolute;
  height: 450px;
  top: -450px;
  left: 0;
  z-index: 10;
`;

const MyCartContainer = styled.div`
  // FIXME: 入っている要素の高さで絶対位置が決まるように修正
  position: absolute;
  height: ${props => props.height}px;
  max-height: 450px;
  top: ${props => {return props.height > 450 ? -440 : props.height * -1 +10}}px;
  right: 0;
  z-index: 10;
`;

const CollectionIcon = styled(Icon)`
  position: relative;
  width: 36px;
  height: 31px;
`;

const BarCodeIcon = styled(Icon)`
  position: relative;
  width: 29px;
  height: 23px;
`;

const ButtonLabel = Label.extend`
  margin: 3px 0 0;
  font-size: ${Const.Size.FONT.SMALL}px;
  color: ${Const.Color.TEXT.WHITE};
`;

const Badge = styled(BadgeComponent)`
  ${props => {return (!props.value > 0) ? 
  `display: none;`:`display:flex;`}}
  position: absolute;
  top: 0;
  right: 0;
`;

const ExtendLandscapeCard = styled(LandscapeCard)`
  margin: 0 0 10px;
`;

const KeywordSearch = styled(KeywordSearchForm)`
`;

const ApplyCartContainer = styled.div``;

const ToMyCollectionButton = Button.extend`
  position: absolute;
  bottom: 10px;
  left: 10px;
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  color: ${Const.Color.TEXT.WHITE};
  justify-content: center;
  border: 6px solid ${Const.Color.DEFAULT};
`;

const OldnijiouButton = Button.extend`
  position: absolute;
  top: -120px;
  right: -70px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${Const.Size.FONT.SMALL}px;
  font-weight: bold;
  line-height: 1.4em;
  color: ${Const.Color.TEXT.WHITE};
  box-shadow: 0 0 8px ${Const.Color.SHADOW.A73BLACK};
  cursor: pointer;
  transition: all 500ms;
  &:hover {
    transform: scale(1.1);
    box-shadow: 0 3px 14px ${Const.Color.SHADOW.A73BLACK};
  }
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(FunctionContainer));