import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import styled, {extend} from 'styled-components'
import {withRouter} from 'react-router-dom'
import {pure} from 'recompose'
import Animate, {Tada} from 'animate-css-styled-components';
import filter from 'lodash/filter'
import {
  addMyCollection,
  calculateTotalAmount
} from '../../../redux/modules/user'

import Const from "../../../const/"
import HighPriceCard from "../../../components/molecules/PriceCard"
import {Label} from '../../../components/atoms/Text'


const HighPriceList = pure(props => {

  return (
    <Container>
      <TitleContainer>
        <Message>期間限定で通常より高く買取できる商品はこちら！</Message>
      </TitleContainer>
      <Inner>
        {
          props.apps.highprice.map((item, index) => {
            const masterDataMatching = filter(props.apps.masterData, o => {return parseInt(o.jan_code) === item.janCode})[0];
            if (masterDataMatching !== undefined) {
              return (
                <div key={`hpc-${index}`}>
                  <ContainerAnimate Animation={[Tada]}
                                    duration={["0.7s"]}
                                    delay={[`${1+0.2*index}s`]}
                  >
                    <HighPriceCard poster={item.figureImg}
                                   price={masterDataMatching.exercise_price}
                                   description={masterDataMatching.product_name}
                                   data={{...masterDataMatching}}
                                   selectKey={index}
                                   up={true}
                                   {...props}
                    />
                  </ContainerAnimate>
                </div>
              )
            }
          })
        }
      </Inner>
    </Container>
  )
});

const mapStateToProps = state => {
  return {
    user: state.user,
    apps: state.apps
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    addMyCollection,
    calculateTotalAmount
  }, dispatch);
}

const Container = styled.div`
  position: relative;
  padding: 20px 20px 140px;
  border-radius: 0 0 5px 5px;
`;

const Inner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: wrap;
`;

const TitleContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  text-align: center;
`;

const Message = Label.extend`
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  color: ${Const.Color.TEXT.SECONDARY};
`;

const ContainerAnimate = styled(Animate)`
  // opacity: 0;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(HighPriceList));