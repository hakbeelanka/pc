import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { withRouter, Link } from 'react-router-dom'
import { pure } from 'recompose'
import Const from '../../../const/'

const GalleryModal = pure(props => {

  return (
    <Container />
  )

});

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

const Container = styled.div`
  position: relative;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(GalleryModal));