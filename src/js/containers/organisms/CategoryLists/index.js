import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

import { pure } from 'recompose'

import Const from "../../../const/"
import CategoryListButton from "../../../components/molecules/CategoryListButton"

const CategoryLists = pure(props => {

  let {history} = props;

  return (
    <Container>
      <Inner>
        <Button
          title={`タイトル`}
          sub={`から探す`}
          bgColor={`A85RED`}
          poster={`/assets/images/category_title.png`}
          onClick={e => {history.push('/categories/titles')}}
          to={'/categories/titles'}
          history={history}
        />
        <Button
          title={`シリーズ`}
          sub={`から探す`}
          bgColor={`A85GREEN`}
          poster={`/assets/images/category_series.png`}
          to={'/categories/series'}
          history={history}
        />
        <Button
          title={`メーカー`}
          sub={`から探す`}
          bgColor={`A85YELLOW`}
          poster={`/assets/images/category_maker.png`}
          to={'/categories/makers'}
          history={history}
        />
      </Inner>
    </Container>
  )
});

const mapStateToProps = state => {
  return {
    player: state.player
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

const Container = styled.div`
  position: relative;
  border-radius: 0 0 5px 5px;
  overflow: hidden;
`;

const Inner = styled.div`
  background-color: ${Const.Color.BACKGROUND.WHITE};
  border-radius: 3px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

const Button = styled(CategoryListButton)`
  display: block;
  width: 100%;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryLists));