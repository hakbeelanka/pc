import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import styled from 'styled-components'
import {withRouter} from 'react-router-dom'
import Const from "../../../const/"
import Balloon from "../../../components/molecules/Balloon";
import Icon from "../../../components/atoms/Icon";
import Button from "../../../components/atoms/Button";
import {
  changeSearchKeyword
} from "../../../redux/modules/apps"

class CategorySearchButton extends Component {

  constructor(props) {
    super(props);
    // FIXME: あとでStateは全部Reduxにまとめる
    this.state = {
      menuIsOpen: [false,false,false],
      currentColor: '',
      categories: [
        {
          key: "titles",
          color: Const.Color.BACKGROUND.PINK,
          text: "タイトルから探す"
        },
        {
          key: "makers",
          color: Const.Color.BACKGROUND.YELLOW,
          text: "メーカーから探す"
        },
        {
          key: "series",
          color: Const.Color.BACKGROUND.GREEN,
          text: "シリーズから探す"
        },
      ]
    }
  }

  componentWillMount() {
    this.state.categories.map((item, index) => {
      if (item.key === window.location.pathname.replace('/categories/', '')) {
        this.setState({currentColor: item.color});
      }
    })
  }

  menuOnMouseEnter (index){
    let newState = this.state.menuIsOpen;
    newState[index] = true;
    this.setState({ menuIsOpen: newState })
  }

  menuOnMouseLeave (index){
    let newState = this.state.menuIsOpen;
    newState[index] = false;
    this.setState({ menuIsOpen: newState })
  }

  render() {
    return (
      <CategorySelectContainer>
        {
          this.state.categories.map((item, index) => {
            return (
              <SelectButtonContainer key={`csel-${index}`}
                                     onMouseEnter={e => this.menuOnMouseEnter(index)}
                                     onMouseLeave={e => this.menuOnMouseLeave(index)}
              >
                {
                  this.state.menuIsOpen[index] ?
                    (
                      <BallonContainer bgColor={Const.Color.BACKGROUND.A80BLACK}
                                       isShadow
                                       arrow={'top'}
                      >
                        {
                          this.props.apps.categoriesData[item.key].map((v,i) => {
                            return (
                              <BalloonList key={`cbal-${i}`}
                                           onClick={e => {
                                             this.props.changeSearchKeyword(v.search_key[0])
                                             this.props.history.push(`/results/${item.key}?cat=${v.link}`)}}
                                           hoverBg={this.state.currentColor}
                              >
                                <SearchIcon search_white />
                                {v.name_ja}
                              </BalloonList>
                            )
                          })
                          // : null
                        }
                      </BallonContainer>
                    ) : null
                }
                <CategorySelectButton rounded width={'151px'} height={'36px'}
                                      bgColor={'transparent'}
                                      onClick={e => this.props.history.push(`/categories/${item.key}`)}
                                      current={item.key === window.location.pathname.replace('/categories/', '')}
                                      categoryColor={this.state.currentColor}
                >{item.text}</CategorySelectButton>
              </SelectButtonContainer>
            )
          })
        }
      </CategorySelectContainer>
    )
  }
};

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    changeSearchKeyword
  }, dispatch)
}

const CategorySelectContainer = styled.div`
  margin: 20px 20px 0;
  width: 500px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const CategorySelectButton = Button.extend`
  border: 1px solid ${props => props.current ? props.categoryColor : Const.Color.BORDER.WHITE};
  font-size: ${Const.Size.FONT.BASE}px;
  justify-content: center;
  font-weight: bold;
  color: ${props => props.current ? props.categoryColor : Const.Color.TEXT.WHITE};
  background-color: ${props => props.current ? Const.Color.BACKGROUND.WHITE : props.categoryColor};
  cursor: pointer;
  &:hover {
    color: ${props => props.categoryColor};
    background-color: ${Const.Color.BACKGROUND.WHITE};  
  }
`;

const SelectButtonContainer = styled.div`
  position: relative;
 
`;

const BalloonList = styled.div`
  position: relative;
  margin: 8px 0 0;
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.WHITE};
  padding: 0 0 0 20px;
  cursor: pointer;
  &:before {
    position: absolute;
    height: calc(100% + 6px);
    width: calc(100% + 8px);
    border-radius: 12px;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
    z-index: -1;
  }
  &:hover {
    &:before {
      content: '';
      background-color: ${props => props.hoverBg};
    }
  }
`;

const BallonContainer = styled(Balloon)`
  position: absolute;
  width: 191px;
  top: 35px;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  z-index: 90;
  flex-direction: column;
  &:before {
    top: -16px;
    font-size: 20px;
  }
`;

const SearchIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 0;
  transform: translateY(-50%);
  width: 13px;
  height: 13px;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(CategorySearchButton));