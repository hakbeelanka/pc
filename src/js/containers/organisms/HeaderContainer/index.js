import React from 'react'
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'
import NavLink from '../../../components/util/ReduceNavLink'
import {
  openMenu
} from '../../../redux/modules/apps'

import { pure } from 'recompose'
import Const from "../../../const/"
import Icon from "../../../components/atoms/Icon"

const HeaderContainer = pure(props => {

  let {history} = props;
  // TODO: 買取王国側が更新した際の情報更新が必要

  return (
    <Container>
      <LogoIcon logo
                onClick={e => props.history.push('/')}
      />
      <Menu>
        <Button exact to={`/`}>トップ</Button>
        <Button exact to={`/highprice/`} new={true}>高価買取アイテム</Button>
        <Button exact to={`/campaign/`} new={true}>キャンペーン</Button>
      </Menu>
      {/*<LoginLink href="https://kaitori-okoku.jp/mousikomi/">*/}
      {/*<SmileIcon smile />*/}
      {/*</LoginLink>*/}
      <MenuIcon menu
                onClick={e => {props.openMenu()}}
      />
    </Container>
  )
});

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    openMenu
  }, dispatch)
}

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 100;
  width: 100%;
  height: 70px;
  border-bottom: 1px solid ${Const.Color.BORDER.GRAY};
  background-color: ${Const.Color.DEFAULT};
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
`;

const Menu = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  width: 1120px;
  height: 100%;
  padding: 0 0 0 145px;
`;

const Button = styled(NavLink)`
  display: flex;
  padding: 0 50px;
  height: 100%;
  justify-content: center;
  align-items: center;
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.DEFAULT};
  text-decoration: none;
  &:before {
    ${props => {return props.new ? "content: 'new'" : null}};
    display: inline-flex;
    justify-content: center;
    align-items: center;
    margin: 0 5px 0 0;
    height: 21px;
    border-radius: 21px;
    padding: 5px 8px;
    font-size: ${Const.Size.FONT.SMALL}px;
    color: ${Const.Color.TEXT.WHITE};
    background-color: ${Const.Color.BACKGROUND.PINK};
  }
  &:hover {
    background-color: ${Const.Color.BACKGROUND.BLUE};
    color: ${Const.Color.TEXT.WHITE};
  }
  &.active {
    background-color: ${Const.Color.BACKGROUND.BLUE};
    color: ${Const.Color.TEXT.WHITE};
  }
`;

const Basics = styled.div`
  position: absolute;
  top: 50%;
  right: 22.5px;
  width: 80px;
  height: 56px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  transform: translateY(-50%);
`;

const MenuIcon = styled(Icon)`
  position: absolute;
  top: 16.5px;
  right: 21px;
  width: 34px;
  height: 40px;
  cursor: pointer;
`;

const LogoIcon = styled(Icon)`
  position: absolute;
  top: 9px;
  left: 11px;
  display: block;
  width: 110px;
  height: 80px;
`;

const SmileIcon = styled(Icon)`
  position: absolute;
  top: 0;
  left: 0;
  width: 35px;
  height: 35px;
`;

const LoginLink = styled.a`
  position: relative;
  display: block;
  width: 35px;
  height: 35px;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderContainer));