import React from 'react'
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'
import NavLink from '../../../components/util/ReduceNavLink'
import {
  closeMenu,
  openTips
} from '../../../redux/modules/apps'
import {
  unflagedWelcome,
  welcomeApps
} from '../../../redux/modules/user'

import { pure } from 'recompose'

import Const from "../../../const/"
import Icon from "../../../components/atoms/Icon"
import {H2} from "../../../components/atoms/Text"
import Button from "../../../components/atoms/Button"

const SideMenu = pure(props => {

  let {history} = props;

  return (
    <Container {...props}>
      <Inner>
        {/*<LoginButton width={`35px`}*/}
                     {/*height={`35px`}*/}
                     {/*circle*/}
        {/*>*/}
          {/*<SmileIcon smile_white />*/}
        {/*</LoginButton>*/}
        <MenuCloseButton width={`35px`}
                         height={`35px`}
                         circle
                         onClick={e => props.closeMenu()}
        >
          <BtnCloseIcon btn_close_menu />
        </MenuCloseButton>
        <Upper>
          <TitleContainer>
            <SearchIcon search_white />
            <Title>買取アイテムを探す</Title>
          </TitleContainer>
          <ExtendNavLink to={'/highprice/'}>オススメ高価買取</ExtendNavLink>
          <ExtendNavLink to={'/categories/titles/'}>タイトルから探す</ExtendNavLink>
          <ExtendNavLink to={'/categories/series/'}>シリーズから探す</ExtendNavLink>
          <ExtendNavLink to={'/categories/makers/'}>メーカーから探す</ExtendNavLink>
          {/*<ExtendNavLink to={'/categories/titles'}>バーコードから探す</ExtendNavLink>*/}
        </Upper>
        <Lower>
          <Link onClick={e => props.history.push('/')}>トップ</Link>
          <Link onClick={e => {
            props.openTips();
            props.unflagedWelcome();
            props.history.push('/')
          }}>にじおうとは</Link>
          <Link onClick={e => props.history.push('/flow')}>宅配買取の流れ</Link>
          <Link onClick={e => window.location ='https://nijigen-okoku.jp/faq/'}>Q&A</Link>
          <Link onClick={e => window.location ='https://kaitori-okoku.jp/contact/?_ga=2.91955481.2003612573.1535636953-1141172974.1533535484'}>お問い合わせ</Link>
          <Link onClick={e => window.location ='https://www.okoku.jp/inc/privacy/'}>プライバシーポリシー</Link>
          <Link onClick={e => window.location ='https://www.okoku.jp/ec/cmSpecificTrant.html'}>特定商取引法に基づく表記</Link>
        </Lower>
      </Inner>
    </Container>
  )
});

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    closeMenu,
    openTips,
    unflagedWelcome,
    welcomeApps
  }, dispatch)
}

const Container = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  z-index: 110;
  width: 260px;
  height: 100%;
  transition: all 500ms;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  box-shadow: -1px 0 1px ${Const.Color.SHADOW.A73BLACK};
  ${props => {return !props.apps.isOpenMenu ?
    `transform: translateX(100%);`: `transform: translateX(0);`
  }}
`;

const Inner = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  overflow-y: scroll;
  width: 100%;
  height: 100%;
`;

const Upper = styled.div`
  position: relative;
  width: 100%;
  padding: 90px 30px 0;
  min-height: 375px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  height: auto;
  &:before {
    position: absolute;
    top: 0;
    left: 0;
    content: '';
    background-color: ${Const.Color.BACKGROUND.D2BLUE};
    opacity: 0.95;
    width: 100%;
    height: 100%;
    z-index: -1;
  }
`;

const Lower = styled.div`
  position: relative;
  width: 100%;
  padding: 30px;
  min-height: 400px;
  height: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  &:before {
    position: absolute;
    top: 0;
    left: 0;
    content: '';
    background-color: ${Const.Color.BACKGROUND.D3BLUE};
    opacity: 0.95;
    width: 100%;
    height: 100%;
    z-index: -1;
  }
`;

const TitleContainer = styled.div`
  position: relative;
  width: 100%;
  padding: 10px 0;
  border-bottom: 1px solid ${Const.Color.BORDER.WHITE};
  margin: 0 0 10px;  
`;

const Title = H2.extend`
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  color: ${Const.Color.TEXT.WHITE};
  padding: 0 0 0 30px;
`;

const SearchIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 3px;
  width: 18px;
  height: 19px;
  transform: translateY(-50%);
`;

const ExtendNavLink = styled(NavLink)`
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.WHITE};
  margin: 20px 0 0;
  text-decoration: none;
  &:hover {
    opacity: 0.8;
  }
  &:first-child {
    margin: 0;
  }
`;

const Link = styled.button`
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.WHITE};
  margin: 20px 0 0;
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }
  &:first-child {
    margin: 0;
  }
`;

const SmileIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 35px;
  height: 35px;
`;

const BtnCloseIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 35px;
  height: 35px;
`;

const LoginButton = Button.extend`
  position: fixed;
  top: 16.5px;
  right: 67px;
  z-index: 10;
`;

const MenuCloseButton = Button.extend`
  position: fixed;
  top: 16.5px;
  right: 19px;
  z-index: 10;
`;

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(SideMenu));