import React, { Component } from "react"
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled,{css} from 'styled-components'
import Slider from 'react-slick'
import campaignData from '../../const/home/campaign_sp'
import Image from '../../components/atoms/Image'
import {
  changeMessage,
  setTipsPosition
} from '../../redux/modules/apps'
import Const from "../../const";
import {Label} from "../../components/atoms/Text";

class Campaign extends Component {

  constructor(props){
    super(props);
    // FIXME: あとでStateは全部Reduxにまとめる
  }

  componentDidMount(){
    this.props.changeMessage(this.props.location.pathname)
  }

  render() {
    return (
      <PageContainer height={this.props.apps.height}>
        <PageInner>
          <MainContentContainer>
            <TitleContainer>
              <Message>キャンペーン</Message>
            </TitleContainer>
            <CampaignSliderInner>
              <Slider {...campaignSliderSetting}>
                {
                  campaignData.map((item, i) => {
                    return (
                      <CampaignItem key={i}
                                    bgimg={item.img}
                                    itemWidth={this.props.itemWidth}
                                    itemHeight={this.props.itemHeight}
                      />
                    )
                  })
                }
              </Slider>
            </CampaignSliderInner>
            <CampaignImage width={`100%`} src={`/assets/images/campaign_01.png`} />
            <CampaignImage width={`100%`} src={`/assets/images/campaign_02.png`} />
            <CampaignImage width={`100%`} src={`/assets/images/campaign_03.png`} />
            <CampaignImage width={`100%`} src={`/assets/images/campaign_04.png`} />
            <CampaignImage width={`100%`} src={`/assets/images/campaign_05.png`} />
            <CampaignImage padding={`0 0 140px`} width={`100%`} src={`/assets/images/campaign_06.png`} />
          </MainContentContainer>
        </PageInner>
      </PageContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    changeMessage,
    setTipsPosition
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Campaign);

const CampaignImage = styled(Image)`
  margin: 15px 0 0;
  ${props => props.padding ? `padding: ${props.padding};` : ``}
`;

const PageContainer = styled.div`
  position: relative;
  margin: 0 auto;
  max-width: 1680px;
  width: 89.6vw;
  min-width: 1120px;
  height: 100%;
  left: 50%;
  transform: translateX(-50%);
`;

const PageInner = styled.div`
  position: absolute;
  max-width: 987px;
  width: 58.75vw;
  min-width: 735px;
  height: calc(100% - 70px);
  top: 0;
  left: 0;
  z-index: 150;
  overflow-y: scroll;
`;

const CampaignSlider = css`
  .slick-dots {
    bottom: 12px;
    li {
      width: 10px;
      height: 10px;
      margin: 0 6px;
      button {
        width: 10px;
        height: 10px;
        &:before {
          width: 10px;
          height: 10px;
          font-size: 10px;
          line-height: 10px;
          color: #fff;
          opacity: 1;
        }
      }
      &.slick-active {
        button {
          &:before {
            color: #4c4c4c;
          }
        }
      }
    }
  }
`;

const MainContentContainer = styled.div`
  width: 100%;
  height: calc(100% + 117px);
  padding: 70px 0 0;
  ${CampaignSlider}
`;

const CampaignSliderInner = styled.div`
  display: block;
  margin: 0 0 -3px;
  border-radius: 5px 5px 0 0;
  overflow: hidden;
`;

const CampaignItem = styled.div`
  background-image: url(${props => {return props.bgimg}});
  background-repeat: no-repeat;
  background-size: contain;
  padding-top: 32%;
`;

const campaignSliderSetting = {
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 4000,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  adaptiveHeight: true,
  centerMode: true,
  centerPadding: '0'
}

const TitleContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  text-align: left;
  padding: 0 0 24px;
`;

const Message = Label.extend`
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.MIDDLE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.SECONDARY};
`;