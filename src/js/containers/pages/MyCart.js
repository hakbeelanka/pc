import React, { Component } from "react"
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
  removeMyCart,
  checkUnopenedCollection,
  uncheckUnopenedCollection
} from "../../redux/modules/user"
import _filter from 'lodash/filter'
import Const from '../../const'
import CountUp from 'react-countup'
import {H2, Label} from '../../components/atoms/Text'
import Button from '../../components/atoms/Button'
import Icon from '../../components/atoms/Icon'
import LandscapeCard from '../../components/molecules/LandscapeCard'
import Balloon from '../../components/molecules/Balloon'
import {filter, map} from "lodash";
import getLevelUp from '../../components/util/getLevelUp'

const Annotations = [
  { "text": "高価買取検索掲載商品" },
  { "text": "化粧箱・フィギュア保護ブリスター・付属品等完品であること" },
  { "text": "正規品であること" },
  { "text": "同一商品は2点まで" },
  { "text": "フィギュア本体の目立つダメージ、付属品不足、ヤニ臭・ヤニ汚れ、化粧箱の大きな損傷が見受けられた場合は対象外となります。" },
  { "text": "抱き枕カバー、シーツ、タペストリー、タオルは定額買取の対象外となります。" }
]

class MyCart extends Component {

  constructor(props){
    super(props);
    // FIXME: あとでStateは全部Reduxにまとめる
    this.state={
    }
  }

  render() {
    return (
      <PageContainer height={this.props.apps.height}>
        <PageInner>
          <PageInfo>
            <PageTitle>うるカート</PageTitle>
            <Appeal>未開封品は表示価格からさらに10%アップします!</Appeal>
          </PageInfo>
          <MainContentContainer>
            <MyCartContainer>
              {
                _filter(this.props.user.collection,(o)=>{ return o.isSell }).map((item, index) => {
                  return (
                    <ExtendLandscapeCard key={`lnd-${index}`}
                                         selectKey={index}
                                         {...this.props}
                                         {...item}
                    />
                  )
                })
              }
            </MyCartContainer>
            <ApplyContainer>
              <TotalAmountContainer>
                {
                  getLevelUp(map(filter(this.props.user.collection,(o)=>{ return o.isSell && parseInt(o.exercise_price) + 1 > 1000 })).length) > 0 ? (
                    <LevelUpBalloon bgColor={Const.Color.BACKGROUND.LYELLOW}
                                    arrow={'bottom'}
                                    isShadow
                    >
                      <LevelUpLabel>レベルアップ！</LevelUpLabel>
                      <LevelUpCount end={getLevelUp(map(filter(this.props.user.collection,(o)=>{
                        return o.isSell && parseInt(o.exercise_price) + 1 > 1000
                      })).length)}
                                    separator=","
                      />
                    </LevelUpBalloon>
                  ) : null
                }
                <TotalAmountLabel>買取総額</TotalAmountLabel>
                <TotalPrice>
                  <CountUp end={(this.props.user.collection.length !== 0 && filter(this.props.user.collection,(o)=>{ return o.isSell }).length !== 0) ? (getLevelUp(map(filter(this.props.user.collection,(o)=>{ return o.isSell && parseInt(o.exercise_price) + 1 > 1000 })).length) + map(filter(this.props.user.collection,(o)=>{ return o.isSell }), getIsUnopenedValue).reduce((accumulator, current) => { return accumulator + current; })) : 0}
                           separator=","
                  />
                </TotalPrice>
              </TotalAmountContainer>
              <ApplyButton rounded
                           bgColor={Const.Color.BACKGROUND.LPINK}
                           width={`286px`}
                           height={`70px`}
                           onClick={e => {window.location = 'https://kaitori-okoku.jp/mousikomi/'}}
              >
                <InboxIcon inbox_white />
                買取申し込みする
              </ApplyButton>
              <AnnotationContainer>
                <AnnotationTitle>うるカートは査定シミュレーションです！<br />
                  実際に申し込みの際に同じものが入っていなくてOKです！
                </AnnotationTitle>
                <AnnotationTitle>定額買取の対象は、下記の条件を満たすものに限ります。</AnnotationTitle>
                <ul>
                  {
                    Annotations.map((item, index) => {
                      return (
                        <Annotation key={`ant-${index}`}>{item.text}</Annotation>
                      )
                    })
                  }
                </ul>
              </AnnotationContainer>
            </ApplyContainer>
          </MainContentContainer>
        </PageInner>
      </PageContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    removeMyCart,
    checkUnopenedCollection,
    uncheckUnopenedCollection
  }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyCart);

const PageContainer = styled.div`
  position: relative;
  margin: 0 auto;
  width 1120px;
  // height: ${props => {return props.height}}px;
  height: 100%;
`;

const PageInner = styled.div`
  position: absolute;
  width: 100%;
  height: calc(100% - 70px);
  top: 0;
  left: 0;
  z-index: 150;
  overflow-y: scroll;
  padding: 20px;
`;

const MainContentContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;

const MyCartContainer = styled.div`
  width: 735px;
  height: calc(100% - 200px);
  padding: 20px;
  overflow-y: scroll;
`;

const ApplyContainer = styled.div`
  width: 385px;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const PageInfo = styled.div`
  width: 100%;
  margin: 0 0 20px;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
`;

const PageTitle = H2.extend`
  color: ${Const.Color.TEXT.PRIMARY};
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.LARGE}px;
  font-weight: bold;
`;

const Appeal = Label.extend`
  padding: 0 0 0 20px;
  color: ${Const.Color.TEXT.SECONDARY};
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  font-weight: bold;
`;

const ExtendLandscapeCard = styled(LandscapeCard)`
  margin: 0 0 10px;
`;

const ApplyButton = Button.extend`
  position: relative;
  color: ${Const.Color.TEXT.WHITE};
  // font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
  font-size: 22px;
  font-weight: bold;
  justify-content: center;
  padding: 0 0 0 40px;
`;

const InboxIcon = styled(Icon)`
  position: absolute;
  width: 40px;
  height: 34px;
  top: 50%;
  left: 20px;
  transform: translateY(-50%);
`;

const TotalAmountContainer = styled.div`
  position: relative;
  width: 286px;
  padding: 0 0 20px;
  margin: 100px 0 20px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  border-bottom: 1px solid ${Const.Color.BORDER.DGRAY};
`;

const TotalAmountLabel = Label.extend`
  font-size: ${Const.Size.FONT.BASE}px;
  color: ${Const.Color.TEXT.PRIMARY};
`;

const TotalPrice = styled.div`
  font-size: ${Const.Size.FONT.HIGH_LARGE}px;
  color: ${Const.Color.TEXT.SECONDARY};
  font-weight: bold;
`;

const AnnotationContainer = styled.div`
  width: 370px;
  margin: 35px 0 0;
`;

const AnnotationTitle = H2.extend`
  font-size: ${Const.Size.FONT.SMALL}px;
  color: ${Const.Color.TEXT.SECONDARY};
`;

const Annotation = styled.li`
  font-size: ${Const.Size.FONT.SMALL}px;
  line-height: 1.2em;
  color: ${Const.Color.TEXT.PRIMARY};
  margin: 8px 0 0;
  &:before {
    content: '☑︎';
    padding: 0 3px 0 0;
    display: inline-block;
  }
`;

const LevelUpBalloon = styled(Balloon)`
  position: absolute;
  top: -80px;
  left: 50%;
  transform: translateX(-50%);
`;

const LevelUpCount = styled(CountUp)`
  font-size: ${Const.Size.FONT.MIDDLE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.SECONDARY};
  &:before {
    content: '¥';
    font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
    color: ${Const.Color.TEXT.SECONDARY};
  }
  &:after {
    content: 'up!';
    font-size: ${Const.Size.FONT.LOW_MIDDLE}px;
    color: ${Const.Color.TEXT.SECONDARY};
  }
`;

const LevelUpLabel = Label.extend`
  font-size: ${Const.Size.FONT.SMALL}px;
  color: ${Const.Color.TEXT.PRIMARY};
  text-align: center;
`;



const getIsUnopenedValue = v => {
  return !v.isUnopened ?
    parseInt(v.exercise_price) :
    parseInt(v.exercise_price) * 1.1;
}