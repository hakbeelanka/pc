import React, { Component } from "react"
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import {
  changeMessage
} from '../../redux/modules/apps'
import CollectionList from '../../containers/organisms/CollectionList'

class MyCollection extends Component {

  constructor(props){
    super(props);
    // FIXME: あとでStateは全部Reduxにまとめる
    this.state={
      threshold: 0,
      contentsOpen: false
    }
  }

  componentDidMount(){
    this.props.changeMessage(this.props.location.pathname)
  }

  render() {
    return (
      <PageContainer height={this.props.apps.height}>
        <PageInner>
          <MainContentContainer>
            <CollectionList />
          </MainContentContainer>
        </PageInner>
      </PageContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    apps: state.apps
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    changeMessage
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyCollection);

const PageContainer = styled.div`
  position: relative;
  margin: 0 auto;
  width 1120px;
  // height: ${props => {return props.height}}px;
  height: 100%;
`;

const PageInner = styled.div`
  position: absolute;
  width: 735px;
  height: calc(100% - 70px);
  top: 0;
  left: 0;
  z-index: 150;
  overflow-y: scroll;
`;

const MainContentContainer = styled.div`
  width: 100%;
  height: 100%;
`;