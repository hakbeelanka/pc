import React, { Component } from "react"
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'
import CategoryCardList from '../../containers/organisms/CategoryCardList'
import CategorySearchButton from '../../containers/organisms/CategorySelectButton'
import isArray from "../../components/util/isArray";

class Categories extends Component {

  constructor(props){
    super(props);
    // FIXME: あとでStateは全部Reduxにまとめる
    this.state={

    }
  }

  render() {
    return (
      <PageContainer height={this.props.apps.height}>
        <PageInner>
          <MainContentContainer>
            <CategorySearchButton {...this.props} />
            <CategoryCardList />
          </MainContentContainer>
        </PageInner>
      </PageContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    apps: state.apps
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories);

const PageContainer = styled.div`
  position: relative;
  margin: 0 auto;
  width 1120px;
  height: 100%;
`;

const PageInner = styled.div`
  position: absolute;
  width: 735px;
  height: calc(100% - 70px);
  top: 0;
  left: 0;
  z-index: 150;
  overflow-y: scroll;
`;

const MainContentContainer = styled.div`
  width: 100%;
  height: 100%;
`;