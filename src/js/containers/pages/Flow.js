import React, { Component } from "react"
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled,{css} from 'styled-components'
import Image from '../../components/atoms/Image'
import {
  changeMessage,
  setTipsPosition
} from '../../redux/modules/apps'
import Const from "../../const";
import {Label} from "../../components/atoms/Text";

class Flow extends Component {

  constructor(props){
    super(props);
    // FIXME: あとでStateは全部Reduxにまとめる
  }

  componentDidMount(){
    this.props.changeMessage(this.props.location.pathname)
  }

  render() {
    return (
      <PageContainer height={this.props.apps.height}>
        <PageInner>
          <MainContentContainer>
            <TitleContainer>
              <Message>宅配買取の流れ</Message>
            </TitleContainer>
            <SpeedLink href={`https://kaitori-okoku.jp/mousikomi/`}>
              <Image src={`/assets/images/speed.png`}
                     width={`100%`}
              />
            </SpeedLink>
            <FlowImage width={`100%`} src={`/assets/images/flow_01.png`} />
            <FlowImage width={`100%`} src={`/assets/images/flow_02.png`} />
            <FlowImage width={`100%`} src={`/assets/images/flow_03.png`} />
            <FlowImage width={`100%`} src={`/assets/images/flow_04.png`} />
            <FlowImage width={`100%`} src={`/assets/images/flow_05.png`} />
            <FlowImage width={`100%`} src={`/assets/images/flow_06.png`} />
            <FlowImage width={`100%`} src={`/assets/images/flow_07.png`} />
            <FlowImage width={`100%`} src={`/assets/images/flow_08.png`} />
            <FlowImage padding={`0 0 140px`} width={`100%`} src={`/assets/images/flow_09.png`} />
          </MainContentContainer>
        </PageInner>
      </PageContainer>
    );
  }
}

const FlowImage = styled(Image)`
  margin: 15px 0 0;
  ${props => props.padding ? `padding: ${props.padding};` : ``}
`;

const SpeedLink = styled.a`
  margin: 0 0 15px;
  display: block;
  width: 100%;
`;

const PageContainer = styled.div`
  position: relative;
  margin: 0 auto;
  max-width: 1680px;
  width: 89.6vw;
  min-width: 1120px;
  height: 100%;
  left: 50%;
  transform: translateX(-50%);
`;

const PageInner = styled.div`
  position: absolute;
  max-width: 987px;
  width: 58.75vw;
  min-width: 735px;
  height: calc(100% - 70px);
  top: 0;
  left: 0;
  z-index: 150;
  overflow-y: scroll;
`;

const MainContentContainer = styled.div`
  width: 100%;
  height: calc(100% + 117px);
  padding: 70px 0 0;
`;

const TitleContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  text-align: left;
  padding: 0 0 24px;
`;

const Message = Label.extend`
  font-family: "waon_joyo", serif;
  font-size: ${Const.Size.FONT.MIDDLE}px;
  font-weight: bold;
  color: ${Const.Color.TEXT.SECONDARY};
`;

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    changeMessage,
    setTipsPosition
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Flow);