import React, { Component } from "react"
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled,{css} from 'styled-components'
import Image from '../../components/atoms/Image'
import Slider from 'react-slick'
import CategoryLists from '../../containers/organisms/CategoryLists'
import campaignData from '../../const/home/campaign_sp'
import {
  changeMessage,
  setTipsPosition
} from '../../redux/modules/apps'

class Home extends Component {

  constructor(props){
    super(props);
    // FIXME: あとでStateは全部Reduxにまとめる
    this.state={
      threshold: 0,
      contentsOpen: false
    }
    this.speedlink = React.createRef()
  }

  componentDidMount(){
    this.props.changeMessage(this.props.location.pathname)
    this.props.setTipsPosition('speedlink', [this.speedlink.current.offsetLeft,this.speedlink.current.offsetTop])
  }

  render() {
    return (
      <PageContainer height={this.props.apps.height}>
        <PageInner>
          <MainContentContainer>
            <div ref={this.speedlink}>
              <SpeedLink href={`https://kaitori-okoku.jp/mousikomi/`}>
                <Image src={`/assets/images/speed.png`}
                       width={`100%`}
                />
              </SpeedLink>
            </div>
            <CampaignSliderInner>
              <Slider {...campaignSliderSetting}>
                {
                  campaignData.map((item, i) => {
                    return (
                      <CampaignItem key={i}
                                    bgimg={item.img}
                                    itemWidth={this.props.itemWidth}
                                    itemHeight={this.props.itemHeight}
                      />
                    )
                  })
                }
              </Slider>
            </CampaignSliderInner>
            <CategoryLists />
          </MainContentContainer>
        </PageInner>
      </PageContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    changeMessage,
    setTipsPosition
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);

const PageContainer = styled.div`
  position: relative;
  margin: 0 auto;
  max-width: 1680px;
  width: 89.6vw;
  min-width: 1120px;
  height: 100%;
  left: 50%;
  transform: translateX(-50%);
`;

const PageInner = styled.div`
  position: absolute;
  max-width: 987px;
  width: 58.75vw;
  min-width: 735px;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 150;
  overflow-y: scroll;
`;

const CampaignSlider = css`
  .slick-dots {
    bottom: 12px;
    li {
      width: 10px;
      height: 10px;
      margin: 0 6px;
      button {
        width: 10px;
        height: 10px;
        &:before {
          width: 10px;
          height: 10px;
          font-size: 10px;
          line-height: 10px;
          color: #fff;
          opacity: 1;
        }
      }
      &.slick-active {
        button {
          &:before {
            color: #4c4c4c;
          }
        }
      }
    }
  }
`;

const MainContentContainer = styled.div`
  width: 100%;
  height: calc(100% + 117px);
  padding: 70px 0 0;
  ${CampaignSlider}
`;

const SpeedLink = styled.a`
  margin: 0 0 15px;
  display: block;
  width: 100%;
`;

const CampaignSliderInner = styled.div`
  display: block;
  margin: 0 0 -3px;
  border-radius: 5px 5px 0 0;
  overflow: hidden;
`;

const CampaignItem = styled.div`
  background-image: url(${props => {return props.bgimg}});
  background-repeat: no-repeat;
  background-size: contain;
  padding-top: 32%;
`;

const campaignSliderSetting = {
  dots: true,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 4000,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  adaptiveHeight: true,
  centerMode: true,
  centerPadding: '0'
}