import React, { Component } from "react"
import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'
import styled,{css} from 'styled-components'
import SearchResultsList from '../../containers/organisms/SearchResultList'
import CategorySearchButton from '../../containers/organisms/CategorySelectButton'

class SearchResult extends Component {

  constructor(props){
    super(props);
  }

  render() {
    const slug = this.props.match.params.id;
    return (
      <PageContainer height={this.props.apps.height}>
        <PageInner>
          <MainContentContainer>
            {/*<CategorySearchButton />*/}
            <SearchResultsList slug={slug}
                               history={this.props.history}
                               {...this.props}
            />
          </MainContentContainer>
        </PageInner>
      </PageContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    apps: state.apps,
    user: state.user
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResult);

const PageContainer = styled.div`
  position: relative;
  margin: 0 auto;
  width 1120px;
  height: 100%;
`;

const PageInner = styled.div`
  position: absolute;
  width: 735px;
  height: calc(100% - 70px);
  top: 0;
  left: 0;
  z-index: 150;
  overflow-y: scroll;
`;

const MainContentContainer = styled.div`
  width: 100%;
  height: 100%;
`;