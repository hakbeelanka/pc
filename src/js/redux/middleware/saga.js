import fetch from 'isomorphic-fetch';
import {call, put, takeEvery} from 'redux-saga/effects'
import {
  REQUEST_FETCH_COLLECTION,
  succeededFetchCollection,
  failedFetchCollection,
  REQUEST_FETCH_CATEGORY,
  succeededFetchCategory,
  failedFetchCategory,
  REQUEST_FETCH_HIGHPRICETOP,
  succeededFetchHighPrice,
  failedFetchHighPrice
} from "../modules/apps";
import {
  updateMasterData
} from "../modules/user";

const pre = 'http://iamas2018.4545.jp'
const develop = 'http://localhost:3000';
const test = 'http://betatest.nijigen-okoku.jp';
const publish = 'http://beta.nijigen-okoku.jp';
const firebase = 'https://nijigen-okoku.firebaseapp.com'

const endPoint = firebase;

function fetchMasterJson() {
  const URL = `${endPoint}/data/master.json`;
  return fetch(URL,{mode:'no-cors'})
    .then(response => response.json())
    .then(json => {
      return json;
    });
}

function fetchCategoryJson() {
  const URL = `${endPoint}/data/categories.json`;
  return fetch(URL,{mode:'no-cors'})
    .then(response => response.json())
    .then(json => {
      return json;
    });
}

function fetchHighPriceTopJson() {
  const URL = `${endPoint}/data/highprice.json`;
  return fetch(URL)
    .then(response => response.json())
    .then(json => {
      return json;
    });
}

function* fetchMasterData(){
  try {
    const payload = yield call(fetchMasterJson);
    yield put(succeededFetchCollection(payload));
    yield put(updateMasterData(payload));
  } catch (e) {
    yield put(failedFetchCollection(e.message))
  }
}

function* fetchCategory(){
  try {
    const payload = yield call(fetchCategoryJson);
    yield put(succeededFetchCategory(payload))
  } catch (e) {
    yield put(failedFetchCategory(e.message))
  }
}

function* fetchHighPriceTop(){
  try {
    const payload = yield call(fetchHighPriceTopJson);
    yield put(succeededFetchHighPrice(payload))
  } catch (e) {
    yield put(failedFetchHighPrice(e.message))
  }
}

function* rootSaga(){
  yield takeEvery(REQUEST_FETCH_COLLECTION, fetchMasterData);
  yield takeEvery(REQUEST_FETCH_CATEGORY, fetchCategory);
  yield takeEvery(REQUEST_FETCH_HIGHPRICETOP, fetchHighPriceTop);
}

export default rootSaga;