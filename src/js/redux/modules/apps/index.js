// status/index.js
import UrumiMessage from '../../../const/home/charactor_message'
import Background from '../../../const/Background'
import filter from 'lodash/filter'

// INITIALIZE
const initialState = {
  width: window.innerWidth,
  height: window.innerHeight,
  masterData: [],
  categoriesData: [],
  highprice: [],
  lastSeachText: "",
  suggestions: [],
  isOpenBarcodeSearch: false,
  isOpenMyCart: false,
  isOpenMenu: false,
  isOpenTips: false,
  characterMessage: "",
  backgroundKey: '',
  tips: []
}

function addItem(array, item) {
  return Array.from(new Set([...array, item]));//重複データが入らないようにするための対応
}

// ACTIONS
// TODO: キャンペーン、高価買取もAJAX取得に変更

export const REQUEST_FETCH_COLLECTION = 'REQUEST_FETCH_COLLECTION';
export const SUCCEEDED_FETCH_COLLECTION = 'SUCCEEDED_FETCH_COLLECTION';
export const FAILED_FETCH_COLLECTION = 'FAILED_FETCH_COLLECTION';

export const REQUEST_FETCH_CATEGORY = 'REQUEST_FETCH_CATEGORY';
export const SUCCEEDED_FETCH_CATEGORY = 'SUCCEEDED_FETCH_CATEGORY';
export const FAILED_FETCH_CATEGORY = 'FAILED_FETCH_CATEGORY';

export const REQUEST_FETCH_HIGHPRICETOP = 'REQUEST_FETCH_HIGHPRICETOP';
export const SUCCEEDED_FETCH_HIGHPRICETOP = 'SUCCEEDED_FETCH_HIGHPRICETOP';
export const FAILED_FETCH_HIGHPRICETOP = 'FAILED_FETCH_HIGHPRICETOP';

export const CHANGE_WINDOW_WIDTH = 'CHANGE_WINDOW_WIDTH';
export const CHANGE_WINDOW_HEIGHT = 'CHANGE_WINDOW_HEIGHT';
export const SELECT_COLLECTION = 'SELECT_COLLECTION';
export const CHANGE_SUGGESTIONS = 'CHANGE_SUGGESTIONS';
export const CHANGE_SEARCH_KEYWORD = 'CHANGE_SEARCH_KEYWORD';

export const OPEN_BARCODE_SEARCH = 'OPEN_BARCODE_SEARCH';
export const CLOSE_BARCODE_SEARCH = 'CLOSE_BARCODE_SEARCH';

export const OPEN_MYCART = 'OPEN_MYCART';
export const CLOSE_MYCART = 'CLOSE_MYCART';

export const OPEN_MENU = 'OPEN_MENU';
export const CLOSE_MENU = 'CLOSE_MENU';

export const CHANGE_MESSAGE_FROM_PATHNAME = 'CHANGE_MESSAGE_FROM_PATHNAME';

export const SET_TIPS_POSITION = 'SET_TIPS_POSITION';
export const OPEN_TIPS = 'OPEN_TIPS';
export const CLOSE_TIPS = 'CLOSE_TIPS';

export const CHANGE_BACKGROUND = 'CHANGE_BACKGROUND';
// REDUCER

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CHANGE_WINDOW_WIDTH:
      return Object.assign({}, state, {width: action.num});
    case CHANGE_WINDOW_HEIGHT:
      return Object.assign({}, state, {height: action.num});
    case SUCCEEDED_FETCH_COLLECTION:
      return Object.assign({}, state, {masterData: action.payload});
    case SUCCEEDED_FETCH_CATEGORY:
      return Object.assign({}, state, {categoriesData: action.payload})
    case SUCCEEDED_FETCH_HIGHPRICETOP:
      return Object.assign({}, state, {highprice: action.payload})
    case CHANGE_SUGGESTIONS:
      return Object.assign({}, state, {suggestions: action.array});
    case CHANGE_SEARCH_KEYWORD:
      return Object.assign({}, state, {lastSeachText: action.text});
    case SELECT_COLLECTION:
      return state;
    case OPEN_BARCODE_SEARCH:
      return Object.assign({}, state, {isOpenBarcodeSearch: true});
    case CLOSE_BARCODE_SEARCH:
      return Object.assign({}, state, {isOpenBarcodeSearch: false});
    case OPEN_MYCART:
      return Object.assign({}, state, {isOpenMyCart: true});
    case CLOSE_MYCART:
      return Object.assign({}, state, {isOpenMyCart: false});
    case OPEN_MENU:
      return Object.assign({}, state, {isOpenMenu: true});
    case CLOSE_MENU:
      return Object.assign({}, state, {isOpenMenu: false});
    case OPEN_TIPS:
      return Object.assign({}, state, {isOpenTips: true});
    case CLOSE_TIPS:
      return Object.assign({}, state, {isOpenTips: false});
    case CHANGE_MESSAGE_FROM_PATHNAME:
      return action.message.length > 0 ?
        Object.assign({}, state, {characterMessage: action.message[0].text}) :
        state;
    case SET_TIPS_POSITION:
      return Object.assign({}, state, {tips: addItem(state.tips, action.tip)});
    case CHANGE_BACKGROUND:
      return Object.assign({}, state, {backgroundKey: action.id});
    default:
      return state;
  }
}


// ACTIONS CREATOR

export function changeWindowWidth(num) {
  return {
    type: CHANGE_WINDOW_WIDTH,
    num
  }
}

export function changeWindowHeight(num) {
  return {
    type: CHANGE_WINDOW_HEIGHT,
    num
  }
}

export function changeSuggestions(array) {
  return {
    type: CHANGE_SUGGESTIONS,
    array
  }
}

export function changeSearchKeyword(text) {
  return {
    type: CHANGE_SEARCH_KEYWORD,
    text
  }
}

export function requestFetchCollection() {
  return {
    type: REQUEST_FETCH_COLLECTION
  }
}

export function succeededFetchCollection(payload) {
  return {
    type: SUCCEEDED_FETCH_COLLECTION,
    payload
  }
}

export function failedFetchCollection(message) {
  return {
    type: FAILED_FETCH_COLLECTION,
    message
  }
}

export function requestFetchCategory() {
  return {
    type: REQUEST_FETCH_CATEGORY
  }
}

export function succeededFetchCategory(payload) {
  return {
    type: SUCCEEDED_FETCH_CATEGORY,
    payload
  }
}

export function failedFetchCategory(message) {
  return {
    type: FAILED_FETCH_CATEGORY,
    message
  }
}

export function requestFetchHighPrice() {
  return {
    type: REQUEST_FETCH_HIGHPRICETOP
  }
}

export function succeededFetchHighPrice(payload) {
  return {
    type: SUCCEEDED_FETCH_HIGHPRICETOP,
    payload
  }
}

export function failedFetchHighPrice(message) {
  return {
    type: FAILED_FETCH_HIGHPRICETOP,
    message
  }
}

export function openBarcodeSearch() {
  return {
    type: OPEN_BARCODE_SEARCH
  }
}

export function closeBarcodeSearch() {
  return {
    type: CLOSE_BARCODE_SEARCH
  }
}

export function openMyCart() {
  return {
    type: OPEN_MYCART
  }
}

export function closeMenu() {
  return {
    type: CLOSE_MENU
  }
}

export function openMenu() {
  return {
    type: OPEN_MENU
  }
}

export function closeTips() {
  return {
    type: CLOSE_TIPS
  }
}

export function openTips() {
  return {
    type: OPEN_TIPS
  }
}

export function closeMyCart() {
  return {
    type: CLOSE_MYCART
  }
}

export function changeMessage(pathname) {
  const message = filter(UrumiMessage, o => {return o.path === pathname});
  return {
    type: CHANGE_MESSAGE_FROM_PATHNAME,
    message
  }
}

export function setTipsPosition(key, pos) {
  const tip = {key: key, pos: pos}
  return {
    type: SET_TIPS_POSITION,
    tip
  }
}

export function setBackground(id) {
  return {
    type: CHANGE_BACKGROUND,
    id
  }
}