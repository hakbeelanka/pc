import _map from 'lodash/map'
import _cloneDeep from 'lodash/cloneDeep'
import _filter from 'lodash/filter'
import _forEach from 'lodash/forEach'

// INITIALIZE
const initialState = {
  name: 'ゲスト',
  isUser: false,
  collection: [],
  total_amount: 0,
  master_version: 0,
  app_version: 1
}

// ACTIONS

export const KEYPRESS_SEARCH_KEYWORD = 'KEYPRESS_SEARCH_KEYWORD';
export const ADD_MYCOLLECTION = 'ADD_MYCOLLECTION';
export const REMOVE_MYCOLLECTION = 'REMOVE_MYCOLLECTION';
export const CALCULATE_TOTAL_AMOUNT = 'CALCULATE_TOTAL_AMOUNT';
export const ADD_MYCART = 'ADD_MYCART';
export const REMOVE_MYCART = 'REMOVE_MYCART';
export const CHECK_UNOPENED = 'CHECK_UNOPENED';
export const UNCHECK_UNOPENED = 'UNCHECK_UNOPENED';
export const UPDATE_COLLECTION_FOR_LOCAL_COLLECTION = 'UPDATE_COLLECTION_FOR_LOCAL_COLLECTION';

export const UPDATE_APP = 'UPDATE_APP';

export const WELCOME_APPLICATION = 'WELCOME_APPLICATION';
export const UNFLAGED_WELCOME = 'UNFLAGED_WELCOME';

// REDUCER

function addItem(array, item) {
  return Array.from(new Set([...array, item]));//重複データが入らないようにするための対応
}

function removeItem(array, item) {
  return array.filter((v, i) => i !== item);
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_MYCOLLECTION:
      return Object.assign({}, state, {collection: addItem(state.collection, action.item)});
    case REMOVE_MYCOLLECTION:
      return Object.assign({}, state, {collection: removeItem(state.collection, action.id)});
    case CALCULATE_TOTAL_AMOUNT:
      const totalAmount = state.collection.length > 0 ? _map(state.collection, 'exercise_price').reduce((accumulator, current) => {
        return parseInt(accumulator) + parseInt(current);
      }) : 0;
      return Object.assign({}, state, {total_amount: totalAmount});
    case KEYPRESS_SEARCH_KEYWORD:
      return state;
    case ADD_MYCART:
      let added_cart = _cloneDeep(state.collection);
      added_cart[action.id].isSell = true;
      return Object.assign({}, state, { collection: added_cart });
    case REMOVE_MYCART:
      let isSellFlg = 0;
      const removed_cart = _cloneDeep(state.collection).map((item) => {
        if(item.isSell){
          if (isSellFlg === action.id){
            item.isSell = false;
          }
          isSellFlg++;
        }
        return item;
      })
      return Object.assign({}, state, { collection: removed_cart });
    case CHECK_UNOPENED:
      let isUnopenFlg = 0;
      const unopened = _cloneDeep(state.collection).map((item) => {
        if(item.isSell){
          if (isUnopenFlg === action.id){
            item.isUnopened = true;
          }
          isUnopenFlg++;
        }
        return item;
      })
      return Object.assign({}, state, { collection: unopened });
    case UNCHECK_UNOPENED:
      let isOpenFlg = 0;
      const opened = _cloneDeep(state.collection).map((item) => {
        if(item.isSell){
          if (isOpenFlg === action.id){
            item.isUnopened = false;
          }
          isOpenFlg++;
        }
        return item;
      })
      return Object.assign({}, state, { collection: opened });
    case WELCOME_APPLICATION:
      return Object.assign({}, state, {isUser: true});
    case UNFLAGED_WELCOME:
      return Object.assign({}, state, {isUser: false});
    case UPDATE_COLLECTION_FOR_LOCAL_COLLECTION:
      let updateCollection = [];
      _cloneDeep(state.collection).map((v, i) => {
        _forEach(action.payload, (_v, _i) => {
          if(v.jan_code === _v.jan_code) {
            v.isUp = (parseInt(_v.exercise_price) > parseInt(v.exercise_price));
            v.exercise_price = _v.exercise_price;
            updateCollection[i] = v;
          }
        })
      })
      return Object.assign({}, state, {collection: updateCollection});
    default:
      return state;
  }
}


// ACTIONS CREATOR
export function addMyCollection(item){
  return {
    type: ADD_MYCOLLECTION,
    item
  }
}

export function addMyCart(id){
  return {
    type: ADD_MYCART,
    id
  }
}

export function removeMyCollection(id){
  return {
    type: REMOVE_MYCOLLECTION,
    id
  }
}

export function removeMyCart(id){
  return {
    type: REMOVE_MYCART,
    id
  }
}

export function calculateTotalAmount(state){
  return {
    type: CALCULATE_TOTAL_AMOUNT
  }
}

export function checkUnopenedCollection(id){
  return {
    type: CHECK_UNOPENED,
    id
  }
}

export function uncheckUnopenedCollection(id){
  return {
    type: UNCHECK_UNOPENED,
    id
  }
}

export function welcomeApps(){
  return {
    type: WELCOME_APPLICATION
  }
}

export function unflagedWelcome(){
  return {
    type: UNFLAGED_WELCOME
  }
}

export function updateMasterData(payload){
  return {
    type: UPDATE_COLLECTION_FOR_LOCAL_COLLECTION,
    payload
  }
}

export function updateApp(){
  return {
    type: UPDATE_APP
  }
}