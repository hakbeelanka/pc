require('babel-register');

const router = require('./src/js/app').default;
const Sitemap = require('./').default;

(
	new Sitemap(router)
		.build('http://beta.nijigen-okoku.jp')
		.save('./sitemap.xml')
);