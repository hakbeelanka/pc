# Nijige-okoku.jp Web App

## Wordpress Login (betatest.nijigen-okoku.jp)

id: kato@hblk.co.jp
 
pw: Yrp!dYypMzO291Tr

## Const

* color -> js/const/Color.js
* size -> js/const/Size.js


## Redux Actions (Ducks design pattern)

### apps (js/redux/modules/apps)

* REQUEST_FETCH_COLLECTION: マスターデータをリクエストする。
* SUCCEEDED_FETCH_COLLECTION: マスターデータ取得成功。
* FAILED_FETCH_COLLECTION: マスターデータ取得失敗。
* REQUEST_FETCH_CATEGORY: カテゴリ情報をリクエストする。
* SUCCEEDED_FETCH_CATEGORY: カテゴリ情報取得成功。
* FAILED_FETCH_CATEGORY: カテゴリ情報取得失敗。
* REQUEST_FETCH_HIGHPRICETOP: 高価買取情報をリクエストする。
* SUCCEEDED_FETCH_HIGHPRICETOP: 高価買取情報取得成功。
* FAILED_FETCH_HIGHPRICETOP: 高価買取情報取得失敗。
* CHANGE_WINDOW_WIDTH: ウィンドウサイズの変更（幅）。
* CHANGE_WINDOW_HEIGHT: ウィンドウサイズの変更（縦）。
* SELECT_COLLECTION: 
* CHANGE_SUGGESTIONS: 検索結果が更新された。
* CHANGE_SEARCH_KEYWORD: キーワード検索の検索ワードが変更された。
* OPEN_BARCODE_SEARCH: バーコード検索を開く。
* CLOSE_BARCODE_SEARCH: バーコード検索を閉じる。
* OPEN_MYCART: うるカートを開く。
* CLOSE_MYCART: うるカートを閉じる。
* OPEN_MENU: メニューを開く。
* CLOSE_MENU: メニューを閉じる。
* CHANGE_MESSAGE_FROM_PATHNAME: キャラクターのメッセージを変化する（URL）
* SET_TIPS_POSITION: TIPSの場所を設定する。
* OPEN_TIPS: TIPSを開く。
* CLOSE_TIPS: TIPSを閉じる。


### apps (js/redux/modules/apps)

* KEYPRESS_SEARCH_KEYWORD: 
* ADD_MYCOLLECTION: コレクションに追加する。
* REMOVE_MYCOLLECTION: コレクションから削除する。
* CALCULATE_TOTAL_AMOUNT: 総額を計算する。
* ADD_MYCART: うるカートに追加する。
* REMOVE_MYCART: うるカートから削除する。
* CHECK_UNOPENED: 新品未開封にする。
* UNCHECK_UNOPENED: 新品未開封を外す
* UPDATE_COLLECTION_FOR_LOCAL_COLLECTION: 更新されたマスターデータを元にコレクションを更新する。 
* UPDATE_APP: 
* WELCOME_APPLICATION: 
* UNFLAGED_WELCOME: 
